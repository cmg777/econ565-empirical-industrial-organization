\input{../slideHeader}
\providecommand{\J}{{\mathcal{J}}}

\title{Models of insurance demand}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}
  \frametitle{References}
  \begin{itemize}
  \item Reviews: \cite{efl2010}, \cite{ef2011}, \cite{ght2015} sections
    6 \& 7
  \item BLP models of insurance demand: \cite{bundorf2012}, \cite{starc2014}
  \item Expected utility models of insurance demand: \cite{cardon2001}, \cite{efrsc2013}
  \item Behavioral: \cite{handel2013}, \cite{barseghyan2013}, \cite{handel2015}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{\cite{starc2014}}

\begin{frame}[allowframebreaks]
  \frametitle{\cite{starc2014}}
  \begin{itemize}
  \item Health insurance industry concentrated
  \item Mergers often blocked by antitrust
    \begin{itemize}
    \item
      \href{http://www.politico.com/story/2017/01/judge-block-aetna-humana-merger-234043}
      {Aetna \& Humana}
    \item
      \href{http://fortune.com/2017/01/19/anthem-cigna-merger-block/}
      {Anthem \& Cigna}
    \end{itemize}
  \item What are the sources and consequences of insurer market power?
  \item Medigap insurance
  \item Estimate model of demand and firm pricing
  \item Results
    \begin{itemize}
    \item Low demand elasticity, strong brand preferences
    \item Average cost pricing would decrease premiums by 17\% 
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Medigap}

\begin{frame}[allowframebreaks] \frametitle{Medigap}
  \begin{itemize}
  \item Medicare has high deductibles \& copays
    \begin{itemize}
    \item Part A (hospitalizatoin) deductible $\approx$\$1000
    \item Part B (outpatient) copays 20\%, no maximum
    \end{itemize}
  \item Medigap provides extra coverage
  \item Set of plans regulated (price [and branding] is only
    characteristic chosen by firms)
  \item Open-enrollment period (within 6 months of enrolling in
    Medicare) price only based on age, gender, state, \& smoking
  \item Minimum Loss Ratio: at least 65\% of premiums must be used to
    cover claims
  \item Taxes vary within consumer state based on insurer state
  \item Data:
    \begin{itemize}
    \item NAIC: insurer premiums, quantities, claims
    \item MCBS: individual demographics, whether have any Medigap (but
      not which insurer \& plan), claims
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab1}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab2}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab3}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab4}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-fig1}
\end{frame}

\subsection{Model}

\begin{frame}[allowframebreaks]\frametitle{Model}
  \begin{itemize}
  \item Firm pricing:
    \begin{align*}
      \max_{p_{jfm}} & \sum_j \left[ \left( p_{jfm} -
                       \underbrace{\gamma_{jfm}(\mathbf{p}_m)}_{\text{claims}} -
                       \underbrace{a_{jfm}(\mathbf{p_m})}_{\text{commissions}} \right) 
                       s_{jfm}(\mathbf{p}_m) M_m \right]  \\
      s.t. & \gamma_{jgm}(\mathbf{p}_m) \geq 0.65 p_{jfm}
    \end{align*}
  \item Demand
    \begin{itemize}
    \item Consumer valuations:
      \[ v_{ijm} = x_j \beta_1 + b_f \beta_2 + x_m \beta_3 + \xi_{jfm}
        + \alpha p_{jfm} + \mu_{ijfm} + \epsilon_{ijfm} \]
    \item $\mu_{ijfm}=$ interactions between $x_j$ and $(z_i, \omega_i)$
    \item Claims:
      \[ \gamma_{ijfm} = \theta_0 + x_j \theta_1 + \underbrace{\omega_i \theta_2}_{\text{income}} +
        \underbrace{z_i \theta_3}_{\text{SRH}} + \varepsilon_{jm} +
        \eta_i \] 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Demand estimation moments:
    \begin{itemize}
    \item BLP market level data: $\Er[\xi_{jfm} | \text{instruments}]
      = 0$
      \begin{itemize}
      \item Retaliatory taxes
      \item Average $p_{jf(-m)t}$
      \end{itemize}
    \item Expected claims given plan:
      \[ \Er[\gamma_{ifm}|J=j] = \theta_0 + x_j \theta_1 + \Er[\omega_i|J=j] \theta_2
        \Er[z_i |J=j] \theta_3 + \varepsilon_{jm} \]
    \item Individual P(any Medigap), premium
    \end{itemize}
  \item Pricing FOC used to estimate marginal costs (commissions)
    \begin{itemize}
    \item Equality if MLR slack, inequality if binding or violated
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab5}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab6}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab7}
\end{frame}

\begin{frame}\frametitle{Consequences of market power}
  \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab8}
\end{frame}

\begin{frame}\frametitle{Consequences of market power}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tab9}
\end{frame}

\begin{frame} \frametitle{Source of market power}
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tabA7}
  
    \includegraphics[width=\textwidth, height=\textheight,
  keepaspectratio=true]
  {starc-tabA8}
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{efrsc2013}}

\begin{frame}
  \frametitle{\cite{efrsc2013}}
  \url{http://faculty.arts.ubc.ca/pschrimpf/565/efrsc-slides_2012_02_18.pdf}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{References}

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}