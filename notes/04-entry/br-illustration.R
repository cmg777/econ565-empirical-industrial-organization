rm(list=ls())
require(ggplot2)

fontFamily <- "Helvetica"
themeLE <- function(base_size=12, base_family=fontFamily) {
  theme_bw() %+replace%
  theme(axis.line = element_line(colour="grey"),
        panel.grid.minor = element_blank(),
        panel.background= element_blank(),
        panel.grid.major.x = element_blank(),
        panel.background = element_rect(fill="white",colour =NA),
        panel.border=element_blank(),
        text = element_text(family=base_family, size=base_size,
          face="plain", hjust=0.5, vjust=0.5, angle=0, lineheight=0.9,
          colour="black"),
        axis.text.x = element_text(angle=60),
        legend.position="bottom")
}
fdims <-1.5*c(5.03937, 3.77953) ## figure dimensions, =128mm x 96mm = dimensions of beamer slide
printDev <- function(filename, width=fdims[1], height=fdims[2], family=fontFamily) {
  cairo_pdf(filename,width=width,height=height,family=family)
  #pdf(filename,width,height)
}

## draws plot for illustrating p-values
graphics.off()
cut <- c(-0.5, 1.0)
epsilon <- c(seq(-4,4,by=0.1), cut)
epsilon <- epsilon[order(epsilon)]
b0 <- 0
bhat <- 1.3
MyDF <- data.frame(epsilon,dnorm(epsilon))
shade <- rbind(c(cut[1],0), subset(MyDF, epsilon >= cut[1] & epsilon<=cut[2]), c(cut[2], 0))

plot <- ggplot() + geom_line(aes(x=epsilon,y=dnorm(epsilon)), color="red") +
  geom_vline(xintercept=cut[1], color="black") +
  geom_vline(xintercept=cut[2], color="black") +
  geom_text(aes(x=cut[1],y=0.3),
            label="cutoff(n+1)") + #expression(-alpha(n+1)S - delta(n+1))) +
  geom_text(aes(x=cut[2],y=0.3),
            label="cutoff(n)") + #expression(-alpha(n)S - delta(n))) +
  geom_segment(aes(x=cut[1],y=0,xend=cut[1],yend=dnorm(bhat))) +
  geom_polygon(data = shade, aes(x=epsilon, y=dnorm.epsilon.), alpha=0.4)
printDev("br-illustration.pdf")
plot
dev.off()
