\documentclass[10pt]{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage[left=.9in,right=.9in,top=.7in,bottom=.7in]{geometry}
%\usepackage[left=1.1in,right=1.1in,top=.7in,bottom=.7in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage[small,compact]{titlesec} 
\usepackage{color}
\usepackage{ulem}
%\usepackage{harvard}
%\renewcommand{\cite}{\citeasnoun}
\usepackage{natbib}
\renewcommand{\cite}{\citet}

%\usepackage[screen,nopanel,gray]{pdfscreen}
%\screensize{4in}{5.33in}
%\margins{0.75in}{0.75in}{0.75in}{0.75in}


%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}

\newcommand{\argmax}{\operatornamewithlimits{arg\,max}}
\newcommand{\argmin}{\operatornamewithlimits{arg\,min}}
\newcommand{\myTable}[1]{\begin{table}[b]\caption{#1}
    \begin{minipage}{\linewidth} \begin{center}}
\newcommand{\myTableEnd}{\end{center}\end{minipage}\end{table}}

\def\inprobLOW{\rightarrow_p}
\def\inprobHIGH{\,{\buildrel p \over \rightarrow}\,} 
\def\inprob{\,{\inprobHIGH}\,} 
\def\indist{\,{\buildrel d \over \rightarrow}\,} 

\newtheorem{theorem}{Theorem}

%\pagestyle{fancy}
\renewcommand{\sectionmark}[1]{\markright{#1}{}}
 
%\fancyhead[LE,LO]{\thepage}
%\fancyhead[CE,CO]{\rightmark}
%\fancyfoot[C]{}

\title{Economics 567 } %-- Market Structure and Business Behaviour}
\date{2017}
\author{Paul Schrimpf \\ \href{mailto:schrimp@mail.ubc.ca}{schrimpf@mail.ubc.ca}}

\makeatletter
\renewcommand{\@maketitle}{
  \null
  \begin{center}%
    {\large \textbf{\@title} \par \normalsize \@date \par \@author}%
  \end{center}%
  \par} 
\makeatother



\begin{document}

\maketitle

This course is about empirical industrial organization. It focuses on
what is known as ``New Empirical Industrial Organization'' --- the use
of structural econometric techniques to study specific markets. Notes
and assignments will be posted on the main course web page at
\url{http://faculty.arts.ubc.ca/pschrimpf/565/565.html}. UBC Connect
will be also be used to post grades and share material that should not
be publicly posted on the web (e.g.\ past referee reports). 

\section{Schedule}
\begin{tabular}{l c c l}
  \hline 
  & \textbf{Day(s)} & \textbf{Time} & \textbf{Location} \\
  Lecture & Tuesday \& Thursday & 9:30am-11:00am  & Iona 533 \\
  Office hours & TBA & & \\
  \hline
\end{tabular} \\
Office hours are subject to change. Any changes in office hours will
be posted on the course web page. If you cannot come to my office
hours, feel free to drop by anytime or email me to schedule an
appointment.  

\section{Course Work}

Course work will consist of a presentation, problem
sets or replication of a paper, and a research proposal. Required
reading should be completed before each lecture. Participation in
class discussion is expected, especially during student
presentations. If there is not enough discussion, weekly summaries of
readings will be required. 

\subsection{Presentation}
Each student will present a paper. The paper should be related to or
on the reading list. Presentations will occur throughout the
term. Each presentation should last for 30 minutes including
questions. The presentation should:
\begin{enumerate}
\item Summarize the paper
\item Identify the paper's contributions
\item Discuss weaknesses of the paper
\item Make suggestions for further research
\end{enumerate}


\subsection{Problem Sets or Replication}
Depending on students preferences and programming experience, there
will either be two or three problem sets that will each involve
reproducing some results from a paper, or a more open-ended
replication of a paper. 

\subsection{Research Proposal} 
A research proposal will be due on the last day of class, November 26th.
Your proposal should clearly state a research question. It should
include a related literature review. It should also include a
description of some of the following: institutional background, data,
and empirical approach. 

\subsection{Grading}
The grade for this course will be 20\% presentation, 40\% problem sets
/ replication, 30\% research proposal, and 10\%
participation. Participation includes attending lecture, contributing
to discussion, attending office hours, and emailing me questions.

\section{Course Outline}

The topics of the course and related readings are listed
below. Additional references can be found in the slides for each
topic. We may not cover all these topics. The order of topics may be
changed depending on class interest. We are unlikely to cover all of
the last four topics. Readings will be added for those four topics if
we get to them. Readings are subject to change.  The notes of
\cite{Aguirregabiria2012} and the {\slshape{Handbook}} chapters by
\cite{reiss2007structural} and \cite{ackerberg2007econometric} provide
good overviews of many of these topics. The references therein are
good sources of further reading.
\begin{enumerate}
\item Introduction 
  \begin{itemize}
  \item Required: \cite{Aguirregabiria2012} chapter 1, \cite{reiss2007structural}
  \item Suggested: \cite{einav2010empirical} 
  \end{itemize}
\item Estimation of production functions 
  \begin{itemize}
  \item Required: \cite{ackerberg2007econometric} section 2, \cite{olley1996dynamics}
  \item Suggested: \cite{levinsohn2003},
    \cite{ackerberg2006structural}, \cite{Aguirregabiria2012} chapter 2,
    \cite{gandhi2009identifying}, \cite{wooldridge2009}
  \end{itemize}
\item Static demand and supply of differentiated products
  \begin{itemize}
  \item Required: \cite{ackerberg2007econometric} section 1, \cite{berry1995}
  \item Suggested: \cite{berry1994}, \cite{Aguirregabiria2012} chapter
    3, \cite{nevo2000practitioner}, \cite{nevo2001}
  \end{itemize}
\item Market entry
  \begin{itemize}
  \item Required: \cite{Aguirregabiria2012} chapter 5, \cite{br1991}, 
  \item Suggested: \cite{br1990}, \cite{seim2006empirical},
    \cite{sweeting2009}, \cite{jia2008happens}
  \end{itemize}
\item Single-agent dynamic structural models
  \begin{itemize}
  \item Required: \cite{rust_chapter_1994}
  \item Suggested: \cite{Aguirregabiria2012} chapters 7, \cite{rust1987}, 
    \cite{hotz1993}, \cite{timmins2002}, \cite{am2002}
  \end{itemize}
\item Dynamic oligopoly
  \begin{itemize}
  \item Required: \cite{ackerberg2007econometric} section 3, \cite{am2010}
  \item Suggested: \cite{Aguirregabiria2012} chapters 6, 9,
    \cite{magnacThesmar2002}, \cite{psd2008}, \cite{bbl2007},
    \cite{pob2007}, \cite{am2007}, \cite{bajari_nonparametric_2009},
    \cite{ryan2012costs}
  \end{itemize}
\item Auctions
\item Contracting and asymmetric information
\item Search and matching
\item Networks
\end{enumerate}

\clearpage
\bibliographystyle{jpe}
\bibliography{../565}

\end{document}

