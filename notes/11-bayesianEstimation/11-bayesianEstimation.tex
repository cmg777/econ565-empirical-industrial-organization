\input{../slideHeader}
\def\Pb{\mathbf{P}}
\def\Lb{\mathbf{\Lambda}}

\providecommand{\J}{{\mathcal{J}}}
\def\Y{\mathcal{Y}}
\def\A{\mathcal{A}}
\def\L{\mathcal{L}}
\def\C{\mathcal{C}}

\title{Bayesian Estimation in IO}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\begin{document}

\frame{\titlepage}

\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{References}
  \begin{itemize}
  \item Brief introductions
    \begin{itemize}
    \item \cite{mikusheva2007} lectures 23-26 (starting slides based off of)
    \item \cite{geweke1999}, \cite{geyer2011}    
    \end{itemize}
  \item Textbooks
    \begin{itemize}
    \item Widely recommended: \cite{gelman2013}
    \item Econometrics focused: \cite{geweke2005},
      \cite{lancaster2004}, \cite{greenberg2012}
    \item Computational \cite{brooks2011}, \cite{marin2007}, \cite{bolstad2011}
    \end{itemize}
  \item Bayesian estimation in IO
    \begin{itemize}
    \item \cite{jiang2009}: BLP
    \item \cite{imai2009}: dynamic discrete choice
    \item \cite{ghk2012}: dynamic game
    \item \cite{norets2013}: dynamic binary choice
    \item \cite{dube2010}: consumer inertia
    \end{itemize}
  \end{itemize} 
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 

\begin{frame} \frametitle{Bayesian econometrics}
  \begin{itemize}
  \item Bayesian econometrics is based on two pieces:
    \begin{enumerate}
    \item A parametric model, giving a distribution, $f(\Y_T|\theta)$, for the data given
      parameters
    \item A prior distribution for the parameters, $p(\theta)$
    \end{enumerate}
  \item Implies
    \begin{itemize}
    \item Joint distribution of the data and
      parameters
      \[ p(\Y_T,\theta) = f(\Y_T|\theta)p(\theta) \]
    \item Marginal distribution of the data
      \[ p(\Y_T) = \int f(\Y_T|\theta)p(\theta) d\theta \]
    \item Posterior distribution of parameters
      \[ p(\theta|\Y_T) = \frac{f(\Y_T|\theta)p(\theta)}{p(\Y_T)} \]
    \end{itemize}
  \item Inference based on posterior
    \begin{itemize}
    \item Report posterior mean (or mode or median) as point estimate
    \item Credible set $=$ set of posterior measure $1-\alpha$  
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Differences between Bayesian and Frequentist Approaches}
  \begin{columns}[T] % align columns
    \begin{column}{.48\textwidth}
      \textbf{Frequentist}
      \begin{itemize}
      \item $\theta$ fixed 
      \item Sample random 
      \item Uncertainty from sampling 
      \item Probability about sampling uncertainty 
      \end{itemize} 
    \end{column}
    \begin{column}{.48\textwidth}
      \textbf{Bayesian}
      \begin{itemize}
      \item $\theta$ random 
      \item  Sample fixed once observed
      \item  Uncertainty from beliefs about parameter 
      \item Probability about parameter uncertainty
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Reasons to be Bayesian}
  \begin{enumerate}
  \item Philosophical
  \item Bayesian methods asymptotically valid from frequentist
    perspective
  \item Decision theory---leads to admissible decision rules
  \item Nuisance parameters easily integrated out
  \item Sometimes easier to implement (main reason for this course)
  \end{enumerate} 
\end{frame}

\subsection{OLS}

\begin{frame}[allowframebreaks] \frametitle{OLS}
  \begin{itemize}
  \item Model $ y_t = x_t \theta + u_t $, $u_t \sim iid N(0,1)$.  
  \item Distribution of data
  \[ f(Y|X,\theta) = (2\pi)^{-T/2} \exp\left(-\frac{1}{2}
    (Y-X\theta)'(Y-X\theta) \right) \]
  \item Conjugate prior (posterior \& prior in same family)
    \begin{itemize}
    \item $\theta \sim N(0,\tau^2 I_k)$,
      \[ p(\theta) = (2\pi \tau^2)^{-k/2} \exp\left(\frac{-1}{2\tau}
        \theta'\theta\right) \]
    \end{itemize}
  \item Posterior
    {\footnotesize{
    \begin{align*}
      p(\theta|Y,X) \propto & \exp\left(-\frac{1}{2} \left[-Y'X\theta -
          \theta'X'Y + \theta'X'X\theta +
          \frac{1}{\tau^2}\theta'\theta\right]\right) \\
      \propto & \exp\left(-\frac{1}{2} \left[-Y'X\theta -
          \theta'X'Y + \theta'(X'X + \frac{I_k}{\tau^2})\theta 
        \right]\right) \\ 
      \propto & \exp\left(-\frac{1}{2}
        \left[ \left(\theta-(X'X+\frac{I_k}{\tau^2})^{-1}X'Y\right)' 
          (X'X+\frac{I_k}{\tau^2})^{-1}
          \left(\theta-(X'X+\frac{I_k}{\tau^2})^{-1}X'Y\right)
        \right]\right) 
    \end{align*}
  }}
    so $\theta|Y,X \sim N(\tilde{\theta},\tilde{\Sigma})$ with 
    \begin{align*}
      \tilde{\theta} = & (X'X+\frac{I_k}{\tau^2})^{-1}X'Y \\
      \tilde{\Sigma} = & (X'X+\frac{I_k}{\tau^2})^{-1}
    \end{align*}
  \item Fix $\tau$ and $T \rightarrow \infty$ with $\frac{X'X}{T} \rightarrow Q_{XX}$, then
    $\tilde{\theta} \rightarrow \theta_0$
  \item Uninformative prior, $\tau \rightarrow \infty$,  $\tilde{\theta}
    \rightarrow (X'X)^{-1}X'Y = \hat{\theta}^{ML}$
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{MCMC}

\begin{frame}\frametitle{MCMC}
  \begin{itemize}
  \item Posterior 
    \[ p(\theta|\Y_T) = \frac{f(\Y_T|\theta)p(\theta)}{p(\Y_T)} =
    \frac{f(\Y_T|\theta) p(\theta)} {\int f(\Y_T|\tilde{\theta}) d\tilde{\theta} }  \]
  \item Closed form posterior is rare, often impossible 
  \item Sample $\theta_i \sim p(\theta|\Y_T)$ instead
  \item Markov Chain Monte-Carlo
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Acceptance-Rejection}
  \begin{itemize}
  \item Want $\xi \sim \pi(x)$, can calculate $f(x) \propto \pi(x)$
  \item Find distribution with pdf $h(x)$ such that $f(x) \leq
    c h(x)$ 
  \item Accept-reject
    \begin{enumerate}
    \item Draw $z \sim h(x)$, $u \sim U[0,1]$
    \item If $u \leq \frac{f(z)}{c h(z)}$, then $\xi = z$.  Otherwise
      repeat (1)
    \end{enumerate}
  \item Let $\rho$ be the probability of rejecting a single draw.  Then,
    {\footnotesize{
        \begin{align*}
          P(\xi \leq x) = & P(z_1 \leq x, u_1 \leq \frac{z_1}{ch(z_1)})
          (1+\rho + \rho^2 + ...) & \\
          = & \frac{1}{1-\rho} P(z_1 \leq x, u_1 \leq \frac{z_1}{ch(z_1)}) &
          \\ 
          = & \frac{1}{1-\rho} E_z \left[P(u \leq
            \frac{z}{ch(z)}|z)\mathbf{1}_{\{z\leq x\}}\right] &
          \\
          = & \frac{1}{1-\rho} \int_{-\infty}^x \frac{f(z)}{ch(z)} h(z) dz &
          \\
          = & \int_{-\infty}^x \frac{f(z)}{c(1-\rho)} dz &
          \\
          = & \int_{-\infty}^x \pi(z) dz &
        \end{align*}
      }}
  \item Advantage: directly gives independent draws from $\pi$
  \item Downside: if $h$ too far from $f$, then will reject many draws
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] 
  \frametitle{Markov Chains}
  \begin{itemize}
  \item  Transition kernel $P(x,A) = $ probability of moving from $x$
    into the set $A$.  
  \item Distribution of $x^k$ is $\pi^*$, then the distribution of $y=x^{k+1}$ is 
    \[ \tilde{\pi}(y) dy = \int_{\Re} \pi^*(x) P(x,dy) dx \]
  \item Invariant measure if $\tilde{\pi} = \pi^*$
  \item Invariant measure exists iff:
    \begin{itemize}
    \item Irreducible: every state can be reached from any other
    \item Positive recurrent: $\Er[$ time until $x$ again $|x]$ finite
    \end{itemize}
  \item Conditions for chain to converge to invariant measure from any
    initial measure:
    \begin{itemize}
    \item Irreducible
    \item Positive recurrent
    \item Aperiodic: greatest common denominator of $\{n: y \text{ can be
        reached from } x \text{ in } n \text{ steps}\}$ is 1
    \end{itemize}
  \item Easier sufficient condition for convergence:
    \begin{itemize}
    \item Reversible: if $\pi(x) p(x,y) = \pi(y)
      p(y,x)$ (aka detailed balance)
    \end{itemize}
  \item Goal: construct a Markov chain, which we can simulate, that
    has the posterior as its invariant measure and has fast mixing
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Metropolis-Hastings}
  \begin{itemize}
  \item General purpose method to sample from $\pi$
    \begin{enumerate}
    \item Draw $y \sim q(x^j,\cdot)$
    \item Calculate $\alpha(x^j,y) = \min \{1, \frac{\pi(y) q(y,x)}{\pi(x) q(x,y)}\}$
    \item Draw $u \sim U[0,1]$
    \item If $u < \alpha(x^j,y)$, then $x^{j+1} = y$.  Otherwise $x^{j+1}
      = x^j$
    \end{enumerate}
  \item Invariant measure is $\pi$
    \begin{itemize}
    \item Proof: detailed balance condition
      \[ \pi(x) q(x,y) \alpha(x,y) = \pi(y) q(y,x) \alpha(y,x) \]
    \end{itemize}
  \item Candidate density $q$
    \begin{itemize}
    \item Too disperse $\implies$ many rejections
    \item Too concentrated $\implies$ high autocorrelation and slow
      mixing (slow convergence to $\pi$)
    \item Common choices:
      \begin{itemize}
      \item Random walk chain: $q(x,y) = q_1(y-x)$, e.g. $y = x +
        \epsilon$, $\epsilon \sim N(0,s)$
      \item Independence chain: $q(x,y) = q_1(y)$
      \item Autocorrelated $y = a + B(x-a) + \epsilon$ with $B<0$
      \end{itemize}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Gibbs sampling}
  \begin{itemize}
  \item Break $x$ into blocks $x
    = (x_1,x_2,...,x_d)$ such that we can draw from
    \[ \pi(x_k|x_1,...,x_{k-1},x_{k+1},...,x_d) \;\; \forall k \]
  \item Simulate
    \begin{itemize}
    \item $x_1^{(j+1)}$ from $\pi(x_1^{(j+1)}|x_2^{(j)},...,x_d^{(j)})$ 
    \item $x_2^{(j+1)}$ from
      $\pi(x_2^{(j+1)}|x_1^{(j+1)},x_3^{(j)},...,x_d^{(j)})$ 
    \item $x_3^{(j+1)}$ from
      $\pi(x_3^{(j+1)}|x_1^{(j+1)},x_2^{(j+1)},x_4^{(j)},...,x_d^{(j)})$ 
    \item ...
    \end{itemize}
  \item Can be viewed as Metropolis-Hastings with $q = \pi(\cdot |
    \cdot)$
  \item Pros:
    \begin{itemize}
    \item Usually fast
    \item No need to choose candidate distribution
    \item Sometimes less autocorrelation
    \item Easy to incorporate latent variables (data augmentation)
    \end{itemize}
  \item Cons:
    \begin{itemize}
    \item Not possible for all models \& priors
    \item Can lead to slow mixing (especially with many blocks)
    \item ``many naive users still have a preference for Gibbs updates that is
      entirely unwarranted. If I had a nickel for every time someone had
      asked for help with slowly converging MCMC and the answer had been to
      stop using Gibbs, I would be rich. Use Gibbs updates only if the
      resulting sampler works well. If not, use something else.''
      \cite{geyer2011}
    \end{itemize}
  \item Example: probit
    \begin{itemize}
    \item $d = \{ x\beta + \epsilon > 0\}$, $\epsilon \sim N(0,1)$
    \item Prior: $\beta \sim N(0, I \tau^2)$
    \item Posterior: $\prod_i \Phi(x_i\beta)^{d_i} \left(1 -
        \Phi(x_i\beta)\right)^{1-d_i}$
    \item Data augmentation: draw $y_i = x_i \beta + \epsilon_i$
      conditional on data and $\beta$
    \item Gibbs sampler:
      \begin{itemize}
      \item Draw $y_i \sim $ truncated $N(x_i\beta, 1; d_i)$
      \item Draw $\beta \sim N\left(X'X+\frac{I_k}{\tau^2})^{-1}X'Y ,
          (X'X+\frac{I_k}{\tau^2})^{-1}\right)$
      \end{itemize}
    \end{itemize}
  \item Example: random coefficients probit (in Bayesian stats,
    random coefficients $\sim$ multilevel model)
    \begin{itemize}
    \item $d_{it} = \{ x_{it}\beta_i + \epsilon_{it} > 0\}$,
      $\epsilon_{it} \sim N(0,1)$, $\beta_i \sim N(\beta, \Sigma)$
    \item Prior: $\beta \sim N(0, I\tau^2)$, $\Sigma^{-1} \sim $
      Wishart $(V)$
    \item Data augmentation: draw $y_{it} = x_{it} \beta_i + \epsilon_{it}$
      conditional on data and $\beta_i$
    \item Gibbs sampler:
      \begin{itemize}
      \item Draw $y_{it} \sim $ truncated $N(x_{it}\beta_i, 1; d_i)$
      \item Draw $\beta_i - \beta \sim N\left(X_i'X_i+ \Sigma)^{-1}X'Y ,
          (X_i'X_i+\Sigma)^{-1}\right)$
      \item Draw $\beta \sim N\left( 1/n \sum \beta_i , S\right)$
      \item Draw $\Sigma^{-1} \sim $ Wishart(something)
      \end{itemize}
    \end{itemize}
  \item Software: OpenBUGS, WinBUGS, JAGS 
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
  \frametitle{Hamiltonian MCMC}
  \begin{itemize}
  \item Improve Metropolis-Hastings through better choice of candidate density
    \begin{itemize}
    \item Avoid high autocorrelation
    \end{itemize}
  \item Overview: \cite{neal2011}
  \item Software: STAN
  \item Hamiltonian dynamics:
    \begin{itemize}
    \item Parameters $=$ position $= x$
    \item Momentum $=m$
    \item Hamiltonian $H(x,m) = U(x) + K(m) = $ potential $+$ kinetic
      energy 
    \item $U(x) = -\log(\pi(x))$
    \item Conservation of energy $\rightarrow$ 
      \begin{align*}
        \frac{dx}{dt} = & \frac{\partial H}{\partial m} \\
        \frac{dm}{dt} = & -\frac{\partial H}{\partial x}
      \end{align*}
      Given $H$ can accurately compute $x(t)$, $m(t)$
    \item Useful properties:
      \begin{itemize}
      \item Symmetrically invertible: $(x^*,m^*) = T_s(x,m) \iff (x,-m) = T_s(x^*,-m^*)$
      \item Conserved: $\frac{dH}{dt}=0$
      \item Volume preserved: mapping $T_s: (x(t),m(t)) \to (x(t+s),m(t+s))$
        has jacobian, $B_s$, with determinant $1$
      \end{itemize}
    \item Use Hamiltonian dynamics for candidate density
    \end{itemize}
  \item Hamiltonian MC for drawing from $\pi$
    \begin{itemize}
    \item Set $U(x) = -\log pi(x)$, $K(m) = m^T M^{-1} m / 2$
    \item Each step of chain: 
      \begin{enumerate}
      \item Draw $m \sim N(0, M)$
      \item Simulate dynamics $(x^*, m^*) = T_s(x,m)$
      \item Accept $x^*$ with probability 
        \[ \alpha(x^*,m^*;x,m) = \min\left \lbrace 1, \exp\left(-U(x^*) + U(x) -
            K(m^*) + K(m) \right) \right \rbrace \]
      \end{enumerate}
    \end{itemize}
  \item Detailed balance: 
    {\footnotesize{
    \begin{align*}
      p(x_1;x_0) \propto & \begin{cases} 0 & \text{ if } (x_1,m_1)
        \neq T_s(x_0,m_0) \text{ for any } m_0,m_1  \\
        e^{-{m_0}^T M^{-1} m_0 / 2} \alpha(x_1,m_1;x_0,m) & \text{ if }
        (x_1,m_1) = T_s(x_0,m_0)  
      \end{cases} \\
      \propto & \begin{cases} 0 &  \text{ if } (x_1,m_1)
        \neq T_s(x_0,m_0) \text{ for any } m_0,m_1  \\
        e^{-K(m_0)} \min\{ 1 , e^{-U(x_1) + U(x_0) - K(m_1) + K(m_0)} \} & \text{ if }
        (x_1,m_1) = T_s(x_0,m_0)  
      \end{cases} 
    \end{align*}
    $\pi(x) = \exp(-U(x))$, and $(x_1,m_1) = T_s(x_0,m_0)$ implies
    $(x_0,m_0) = T_s(x_1,-m_1)$, so
    \begin{align*}
      \pi(x_0) p(x_1;x_0) = & e^{-U(x_0)-K(m_0)} \min\{ 1 ,
      e^{-U(x_1) + U(x_0) - K(m_1) + K(m_0)} \} \\
      = &  e^{-U(x_1)-K(-m_1)} \min\{ 1 ,
      e^{-U(x_0) + U(x_1) - K(m_0) + K(-m_1)} \} \\
      = & \pi(x_1,m_1) p(x_0,m_0;x_1,m_1) 
    \end{align*}
    }}
  \item Tuning choices:
    \begin{itemize}
    \item Length of path to simulate, $s$, which in practice is
      discretized into $L$ steps of size $\epsilon$
    \item Variance of momentum, $M$
    \item Various methods to automate e.g.\ NUTS
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Example: Probit}
  \href{https://bitbucket.org/paulschrimpf/econ565/src/master/notes/11-bayesianEstimation/mcmc-probit.R?at=master}
  {R code}
  
  \includegraphics[width=\linewidth]{chains}
  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{IO applications}

\subsection{\cite{jiang2009}}

\begin{frame}\frametitle{\cite{jiang2009}} 
  \begin{itemize}
  \item Bayesian BLP
  \item BLP uses moment conditions
    \begin{itemize}
    \item Consumer $i$ utility from buying product $j$ in market $t$
      \begin{align*}
        u_{ijt} = & \overbrace{x_{jt}}^{1 \times K}
        \overbrace{\underbrace{\theta_{it}}_{=\bar{\theta} + \nu_{it}}}^{K \times 1} + \overbrace{\xi_{jt}}^{1 \times 1} + \epsilon_{ijt}   \\
      \end{align*}
    \item Common demand shock $\xi_{jt}$ endogenous, have instruments $w$
      \[ \Er[\xi_{jt} | w_{jt} ] = 0 \]
    \end{itemize}
  \item Bayesian needs likelihood, so assume
    \[ x_{jt} = w_{jt} \delta + u_{jt} \]
    and 
    \[ \begin{pmatrix} 
      u_{jt} \\ \xi_{jt} 
    \end{pmatrix} 
    \sim N(0,\Omega). \]
  \end{itemize}  
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Model \& likelihood}
  \begin{itemize}
  \item Utility:
    \begin{align*}
      u_{ijt} = & \overbrace{x_{jt}}^{1 \times K}
      \overbrace{\underbrace{\theta_{it}}_{=\bar{\theta} + \nu_{it}}}^{K \times 1} + \overbrace{\xi_{jt}}^{1 \times 1} + \epsilon_{ijt}   \\
    \end{align*}  
  \item First stage
    \[ x_{jt} = w_{jt} \delta + u_{jt} \]
  \item Distributional assumptions:
    \begin{itemize}
    \item $\epsilon_{ijt} \sim$ type I extreme value
    \item $\nu_{it} \sim N(0,\Sigma)$, i.i.d across $i$, $t$
    \item $\begin{pmatrix} 
        u_{jt} \\ \xi_{jt} 
      \end{pmatrix} 
      \sim N(0,\Omega)$, i.i.d across $j$, $t$
    \end{itemize}
  \item Share equation:
    \begin{align*} s_{jt} = & \int \frac{ \exp(x_{jt}(\bar{\theta} + \nu) +
        \xi_{jt}) } { 1 + \sum_{k=1}^j \exp(x_{kt}(\bar{\theta} + \nu) +
        \xi_{kt}) } dF_\nu(\nu;\Sigma) \\ = & h(\xi_t|x_t, \bar{\theta},
      \Sigma) 
    \end{align*}
  \item Likelihood:
    \[
    \pi(s_t , x_t|w_t, \bar{\theta}, \Sigma, \delta, \Omega) = 
    \phi\left( \begin{pmatrix}
        h^{-1}(s_t | x_t, \bar{\theta}, \Sigma) \\
        x_t - w_t \delta 
      \end{pmatrix}
      |\Omega\right) (J_{s_t \to \xi_t})^{-1}
    \]
    where $J_{s_t \to \xi_t} = $ determinate of $ds_t/d\xi_t$
  \item $J_{s_t \to \xi_t}$ given shares is function of only $\Sigma$    
  \end{itemize}  
\end{frame}

\begin{frame}\frametitle{Prior}
  \begin{itemize}
  \item $\bar{\theta} \sim N(\theta_0, V_\theta)$
  \item $\delta \sim N(\delta_0, V_\delta)$
  \item $\Omega \sim $ inverse Wishart$(\nu_0, V_\Omega)$
  \item $\Sigma = U'U$, $U = \begin{pmatrix} e^{r_{11}} & r_{12} & \cdots
      & r_{1K} \\
      0 & e^{r_{22}} & r_{23} & \ddots & \vdots \\
      \vdots &     & \ddots &  & \vdots \\
      0 & \cdots & \cdots & e^{r_{kk}} \end{pmatrix}$
    \begin{itemize}
    \item $r_{jj} \sim N(0,\sigma_{r_{jj}}^2)$
    \item $r_{jk} \sim N(0,\sigma_{r_{off}^2})$ for $j<k$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{MCMC}
  \begin{itemize}
  \item Combination of Gibbs and random walk Metropolis Hastings
  \item Gibbs sampler for $\bar{\theta}, \delta, \Omega | r, s, x, w,
    $ priors
  \item Metropolis for $\Sigma = r | \bar{\theta}, \delta, \Omega, r,
    s, x, w$
    \begin{itemize}
    \item Candidate density: $r^{new} = r^{old} + N(0, \sigma^2 D_r)$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Advantages}
  \begin{itemize}
  \item No maximization (but problem of chain convergence instead)
  \item Simulations show lower MSE than GMM (even in simulations with
    $\xi$ not normally distributed
  \item Inference natural by-product of MCMC
    \begin{itemize}
    \item No extra work needed to compute standard errors
    \item Inference on functions of parameters straightforward, no
      need for delta-method
      \begin{itemize}
      \item Sample from posterior of $f(\theta)$ by drawing $\theta$
        from posterior and calculating $f(\theta)$
      \item e.g.\ elasticities, any counterfactuals, etc        
      \end{itemize}
    \end{itemize}
  \item Simulations GMM asymptotic confidence intervals too small,
    MCMC gets correct coverage of credible regions
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Simulation results}
  \begin{itemize}
  \item $J=3$, $T=300$, $K=4$ (brand effects and price)
  \item Setup 1: 
    \begin{itemize}
    \item no endogeneity $x = w$
    \item Distributions for $\xi$
      \begin{itemize}
      \item Correctly specified: $\xi \sim N(0,1)$
      \item Heteroskedasticity: $\xi \sim N(0, \exp(-.5413 +
        x^p_{jt}))$
      \item AR(1) $\xi_{jt} = \rho\xi_{jt-1} + N(0,v)$
      \item Different distributoin: $\xi \sim $ Beta with parameters
        such that either symmetric or assymetric
      \end{itemize}      
    \end{itemize}
  \item Setup 2:
    \begin{itemize}
    \item One component of $x$ endogenous, $w=$ exogenous $x$'s and
      one instrument
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight]{jmr-tab1}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight]{jmr-tab2}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight]{jmr-tab7}
\end{frame}

\begin{frame}\frametitle{Coverage of confidence intervals}
  \begin{itemize}
  \item In setup 1 with correctly specified distribution, GMM 95\%
    confidence intervals have 63\% coverage
  \item In setup 1 with correctly specified distribution, Bayesian 95\%
    credible intervals have 81\% coverage
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Distributional assumption about $\xi$}
  \begin{itemize}
  \item Why does distributional assumption on $\xi$ not seem to
    matter?
  \item Recall that Bayesian OLS with normal distribution $\to$
    frequentist OLS
    \begin{itemize}
    \item Ignoring priors, gradient of posterior $=$ moment conditions 
    \end{itemize}
  \item Same reasoning implies IV with normal distributions $\to$
    frequentist IV
    \begin{itemize}
    \item LIML \& FIML consistent because gradient of posterior $=$
      moment conditions
    \end{itemize}
  \item Based on this conjecture that misspecified shape of
    distribution is fine, but misspecifying heteroskedasticity or
    dependence should lead to consistent estimates, but inconsistent
    inference
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Allowing heteroskedasticity \& dependence}
  \begin{enumerate}
  \item Specify distribution of $\xi$ more flexibly
    \begin{itemize}
    \item E.g. Dirichlet process see \cite{conley2008}
    \end{itemize}
  \item \cite{chernozhukov2003}: quasi-Bayesian estimation
    \begin{itemize}
    \item Moments $\Er[m_i(\theta)] = 0$, let $g_n(\theta) =
      \frac{1}{n} \sum_i m_i(\theta)$, $W_n(\theta) =$ consistent
      estimate of $\lim_{n \to \infty} \mathrm{Var}(\sqrt{n}
      g_n(\theta))$
    \item Quasi-posterior: $\propto \exp(-n/2 g_n(\theta)'W_n(\theta)
      g_n(\theta))$
    \item Bayesian estimation and inference using quasi-posterior is
      consistent
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}\frametitle{Applied papers}
  \begin{itemize}
  \item \cite{cohen2013}: vertical supplier relationship's effects on
    milk prices
  \item \cite{musalem2010}: effect of out-of-stock
  \item 
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\cite{imai2009}}
\begin{frame}[shrink]\frametitle{\cite{imai2009} -- ``Bayesian estimation of
    dynamic discrete choice models''}
  \begin{itemize}
  \item Recall likelihood for dynamic discrete choice:
    \[ \sum_{t=1}^{T} \sum_{i=1}^N
    \log \Lambda\left(a_{it} | v_i^{\Pb}(\cdot, x_{t};\theta)
    \right) \]
    where $\Pb = \Lb(v^{\Pb}(\theta))$
  \item Na\:{i}ve Metropolis-Hastings: 
    \begin{itemize}
    \item Draw candidate $\theta$
    \item Solve for value function
    \item Accept or reject with some probability
    \end{itemize}
  \item Typically infeasible because solving for value function takes
    too long
  \item Idea of this paper: combine MCMC iterations with value
    function iterations 
  \item Each Metropolis step, do one Bellman iteration to update value
    function     
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Model}
  \begin{itemize}
  \item State = $s$ (observed) \& $\epsilon$ (unobserved)
  \item Parameters $\theta$
  \item Value function:
    \begin{align*}
      V(s,\epsilon,\theta) = & \max_{a \in A} R(s,a,\epsilon_a,\theta)
      + \beta \Er_{\theta_s}[V(s',\epsilon',\theta) | s,a] \\
      = & \max_{a \in A} v(s,a,\epsilon_a,\theta)
    \end{align*}
  \item Choice probabilities
    \[ \Pr[a=a_{i,t}|s_{i,t}, V, \theta] = \Pr[\epsilon : a_{it} =
    \argmax v(s,a,\epsilon_a,\theta)] \]
  \item State transition pmf $f(s'|s,a;\theta_s)$
  \item Conditional likelihood:
    \[ L(Y | \theta) = \prod_{i,t} \Pr[a=a_{i,t}|s_{i,t}, V,
    \theta] \]
  \end{itemize}  
\end{frame}

\begin{frame}\frametitle{Bayesian DP}
  \begin{itemize}
  \item $\theta_s$ estimated separately? Paper is unclear, but fine
  \item Iteration $t$, draw $\theta^{*t}
    \sim q(\theta^{t-1},\cdot)$ 
  \item At iteration $t$, have history of draws of $V^\tau,
    \epsilon^\tau, \theta^{*\tau}$ for $\tau < t$
  \item Expected value:
    {\footnotesize{
    \[ \hat{\Er}^t[V(s',\epsilon',\theta^*)|s,a] = \sum_{s'}
    f(s'|s,a,\theta) \begin{pmatrix} \sum_{n=1}^{N(t)}
      V^{t-n}(s',\epsilon^{t-n}, \theta^{*(t-n)}) \times \\
      \\ \times \frac{K_h(\theta^* - \theta^{*(t-n)})} 
      {\sum_{k=1}^{N(t)} K_h(\theta^* - \theta^{*(t-k)})} \end{pmatrix} \]
    }}
  \item Choice specific:
    \[ v(s,a,\epsilon_a,\theta^*) = R(s,a,\epsilon_a,\theta^*) + \beta
    \hat{\Er}^t[V(s',\epsilon',\theta^*)|s,a] \]
  \item Accept or reject $\theta^*$ based on likelihood using
    $v(s,a,\epsilon_a,\theta^*)$
  \item Draw $\epsilon^t \sim F(\epsilon;\theta^*_\epsilon)$ calculate
    and save
    \[ V^t(s,\epsilon^t,\theta^{*t}) = \max_{a \in A}
    v(s,a,\epsilon^t_a,\theta^{*t}) \]
  \item Flow chart from \cite{ciij2012} ``Practitioner's guide ...''
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight]{icj-flow1}
\end{frame}

\begin{frame}\frametitle{Statistical properties}
  \begin{itemize}
  \item Theorem 1: $\hat{\Er}^t[V] \inprob\Er[V]$ uniformly over $s$,
    $\theta$, as $t \to \infty$
  \item Theorem 2: $\theta^{(t)} \inprob \tilde{\theta}^{(t)}$ where
    $\tilde{\theta}^{(t)}$ is Markov chain generated by usual
    Metropolis-Hastingings
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Extensions}
  \begin{itemize}
  \item Continuous $s$: draw $s^t$ along with $\epsilon^t$, add
    importance weights to $\hat{\Er}^t$
  \item Unobserved heterogeneity: Metropolis draws for each $i$, Gibbs
    updating for hyperparameters
  \item \cite{norets2009}: uses nearest-neighbor instead of kernel
    approximation to $V$
    \begin{itemize}
    \item Incorporates serially correlated unobservables
    \item Argues more computationally efficient and also applicable to
      more general model specifications 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[plain]
  \includegraphics[height=\textheight]{icj-flow2}
\end{frame}

\begin{frame}\frametitle{Applied papers}
  \begin{itemize}
  \item \cite{ishihara2012}: dynamic demand
  \item \cite{toubia2013}: contributing to Twitter
  \end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}