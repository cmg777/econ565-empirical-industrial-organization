\input{../slideHeader}

\title{Introduction to empirical industrial organization}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\begin{document}

\frame{\titlepage}
%\setcounter{tocdepth}{2}

\begin{frame}
  \tableofcontents  
\end{frame}

\begin{frame}\frametitle{References}
  \begin{itemize}
  \item \cite{Aguirregabiria2012} chapter 1
  \item \cite{reiss2007structural} through section 4
  \item \cite{einav2010empirical} 
  \end{itemize}
\end{frame}

\section{Questions}

\begin{frame}
  \frametitle{Industrial organization}
  Industrial organization is about the structure of
  industries in the economy and the behavior of firms and
  individuals in these industries
  \begin{itemize}
  \item Departures from perfect competition
    \begin{itemize}
    \item Strategic behavior
    \item Scale economies
    \item Transaction costs
    \item Information frictions
    \end{itemize}
  \item Impact on firms' profits and consumers' welfare
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{General Approach}
  \begin{itemize}
  \item Goal: model how profits and welfare are influenced by
    ``exogenous'' factors such as: 
    \begin{itemize}
    \item Demand
    \item Technology
    \item Institutional features and regulation
    \end{itemize}
  \item Also interested in:
    \begin{itemize}
    \item \textbf{Market structure}: number of firms and their
      respective market shares
    \item \textbf{Market power}: ability of firms to earn
      extraordinary profits 
      \note{Each range from perfect competition
        to monopoly. These things were focused on historically because
        market structure is easily measured and market power is
        conceptually clear. However, most policy questions for both
        firm managers and regulators are more directly about profits
        and welfare than about market structure and power.}
    \end{itemize}
  \item Useful for:
    \begin{itemize}
    \item Firm managers for e.g.\ choosing prices, evaluating a
      merger, predict the effect of introducing a new product, etc.
    \item Governments for e.g.\ choosing how to regulate natural
      monopolies, identifying and punishing anti-competitive behavior,
      predicting the effects of taxes and environmental policy, etc
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Examples}

\begin{frame}
  \frametitle{Example 1}

  \begin{itemize}
  \item  These three examples come from Aguirregabiria (2012)
  \item \textbf{New product}: A company is considering launching a new
    product, e.g., a new smartphone.
    \begin{itemize}
    \item Goal: choose price and estimate profits
    \item Needs to predict demand and response of other firms
    \item Data on sales, prices, and product attributes along with
      methods from this course can be used
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example 2}
  \begin{itemize}
  \item \textbf{Environmental policy}: A government imposes new
    restrictions on the emissions of pollutants from factories in an
    industry. 
    \begin{itemize}
    \item New policy encourages adoption of a new cleaner technology 
    \item Changes cost structure, which will affect competition
    \item E.g.\ if the new technology reduces variable costs but
      increases fixed costs, then expect a decline in the number of
      firms and an increase in the average size (output) of a firm in
      the industry
    \item Data on prices, quantities, and number of firms in the
      industry, together with a model of oligopoly competition, we can
      evaluate the effects of this policy change in the industry on
      both firms and consumers
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Example 3}
  \begin{itemize}
  \item \textbf{Intel-AMD duopoly}: in the CPU market has lasted many
    years with clear leadership by Intel with more than two-thirds
    market share.
    \begin{itemize}
    \item Questions: why has market structure and market power been so
      persistent? 
    \item Possibilities: large sunk entry costs and economies of scale,
      learning-by-doing, consumer brand loyalty, or predatory conduct
      and entry deterrence
    \item Data on prices, quantities, product characteristics, and
      firms’ investment in capacity allow us to measure the
      contribution of these factors
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{Methodology}

\begin{frame}
  \frametitle{Methodology}
  Empirical industrial organization \note{or at least the ``New
    Empirical Industrial Organization'' which has been the most
    influential approach for the past 20 years} has a distinct
  methodology focusing on structural economic models
  \begin{itemize}
  \item Complete economic model tailored to the question and industry
    being studied
  \item Econometric model closely tied to economic model
  \item Trade off between breadth of questions you can answer and
    strength of assumptions 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Historical approaches to empirical IO}
  \begin{itemize}
  \item 1940s and earlier: case studies
    \begin{itemize}
    \item Careful descriptions of specific industries, firms, or
      events 
    \item Little quantification or formal tie to theory
    \end{itemize}
  \item 1950s-1970s: structure-conduct-performance
    \begin{itemize}
    \item Cross-industry regressions relating market structure to
      market outcomes
    \item E.g.\ regress Lerner index, $(P-MC)/P$, on
      Herfindahl-Hirschman index, $\sum_{i=1}^N \mathrm{share}_i^2$
    \item Drawbacks: (1) ignores industry heterogeneity, (2) does not
      identify causal effect      
    \end{itemize}
  \item Late 1980s-present: new empirical industrial organization
    \begin{itemize}
    \item Analyses of individual industries
    \item Empirical analysis framed in terms of an economic theory of
      the relevant industry or a set of competing theories
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Structural empirical models in IO}

\begin{frame}
  \frametitle{Ingredients of a structural economic model in IO}
  \begin{enumerate}
  \item Question
  \item Economic model
    \begin{itemize}
    \item Key features of the industry that are important to answer
      our empirical question
    \item Should not be needlessly complicated
    \end{itemize}
  \item Data
  \item Econometric specification of model
    \begin{itemize}
    \item Economic models are deterministic and will never match data,
      so need to add heterogeneity and/or shocks
    \end{itemize}
  \item Estimation
  \item Reporting of results
  \end{enumerate}
\end{frame}  


\subsection{Economic Model}

\begin{frame}[allowframebreaks]
  \frametitle{Model ingredients}
  \note{From Reiss and Wolak (2007).}
  \begin{enumerate}
  \item Description of the economic environment
    \begin{enumerate}
    \item the extent of the market and its institutions;
    \item the economic actors; and
    \item the information available to each actor.
    \end{enumerate}
  \item List of primitives
    \begin{enumerate}
    \item technologies (e.g., production sets);
    \item preferences (e.g., utility functions); and
    \item endowments (e.g., assets).
    \end{enumerate}
  \item Variables exogenous to agents and the economic environment
    \begin{enumerate} 
    \item constraints on agents’ behavior; and
    \item variables outside the model that alter the behavior of
      economic agents
    \end{enumerate}
  \item Decision variables, time horizons and objective functions of
    agents, such as:
    \begin{enumerate}
    \item utility maximization by consumers and quantity demanded; and
    \item profit maximization by firms and quantity supplied.
    \end{enumerate}
  \item An equilibrium solution concept, such as:
    \begin{enumerate}
    \item Walrasian equilibrium with price-taking behavior by
      consumers; and
    \item Nash equilibrium with strategic quantity or price
      selection by firms.
    \end{enumerate}
  \end{enumerate}
\end{frame}
  

\subsection{Econometric specification}

\begin{frame}[allowframebreaks]
  \frametitle{Econometric specification}
  \begin{itemize}
  \item Economic models are deterministic and will never match data,
    so need to add heterogeneity and/or shocks    
    \begin{itemize}
    \item Unobserved heterogeneity
      \begin{itemize}
      \item E.g.\ firms vary in their productivity
      \item Must clearly specify to whom what is observed/unobserved ---
        e.g.\ all firms' productivities are unobserved by the
        econometrician, and firms observe their own productivity but not
        others 
      \end{itemize}
    \item Optimization errors
      \begin{itemize}
      \item Agents fail to exactly maximize their payoffs
      \end{itemize}
    \item Measurement errors
    \end{itemize}    
  \item Functional forms and distributional assumptions
    \begin{itemize}
    \item Economic models involve utility, profit, etc. functions of
      unknown form. For estimation we often restrict functions and
      distributions to be of a known parametric form.
      \begin{itemize}
      \item Reasons: (i) computational tractibility, (ii) limited
        data size, (iii) identification (often questionable)
      \item E.g.\ utility CRRA, Cobb-Douglas production function,
        prodictivity log-normal, etc
      \end{itemize}
    \end{itemize}
  \item Identification: given the distribution of the observed data,
    is there a unique value of model parameters that match that
    distribution?
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Example: {\cite{br1991}}}

\begin{frame} \frametitle{Example: \cite{br1991}}
  \begin{itemize}
  \item Can learn a lot from market entry with very limited data 
  \item Cross-section of isolated markets where we observe
    \begin{itemize}
    \item Number of firms
    \item Some market characteristics (prices and quantities not
      needed) 
    \end{itemize}
  \item Identify:
    \begin{itemize}
    \item Fixed costs
    \item Degree of competition: payoffs $=f(\text{number of firms})$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Setting}
  \begin{itemize}
  \item Questions:
    \begin{itemize}
    \item Degree of competition: how fast profits decline with $n_m$
    \item How many entrants needed to achieve competitive equilibrium
      (contestable markets)
    \end{itemize}
  \item Data:
    \begin{itemize}
    \item Retail and professional industries (doctors, dentists,
      pharmacies, car dealers, etc.), treat each industry separately
    \item $M$ markets 
    \item $n_m$ firms per market
    \item $S_m$ market size
    \item $x_m$ market characteristics
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Model}
  \begin{itemize}
  \item $N$ potential entrants
  \item Profit of each firm when $n$ active $= \Pi_m(n)$
    \begin{itemize}
    \item $\Pi_m$ decreasing in $n$
    \end{itemize}
  \item Equilibrium: 
    \[ \Pi_m(n_m) \geq 0 \;\text{ and }\; P_m(n_m+1) < 0 \]
  \item Profit function:
    \begin{align*} 
      \Pi_m(n) = & \underbrace{V_m(n)}_{\text{variable}} -
      \underbrace{F_m(n)}_{\text{fixed}}  \\
      = & S_m v_m(n) - F_m(n) \\
      = & S_m \left(x_m^D \beta - \alpha(n) \right) -
      \left(x_m^c\gamma + \delta(n) + \epsilon_m \right) 
    \end{align*}
    where
    \begin{itemize}
    \item $\alpha(1) \leq \alpha(2) \leq \cdots \leq \alpha(N)$
    \item $\delta(1) \leq \delta(2) \leq \cdots \leq \delta(N)$
      \begin{itemize}
      \item Entry deterrence, firm heterogeneity, real estate prices
      \end{itemize}
    \item Key difference between variable and fixed profits is that
      variable depend on $S_m$, fixed do not
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{Estimation}
  \begin{itemize}
  \item Parameters $\theta = (\beta,\gamma,\alpha,\delta)$
  \item MLE
    \[ \hat{\theta} = \argmax_\theta \sum_{m=1}^M \log \Pr(n_m | x_m,
    S_m; \theta) \]
  \item Assume $\epsilon_m \sim N(0,1)$, independent of $x_m,S_m$ 
    {\footnotesize{
    \begin{align*}
      \Pr(n | x_m, S_m; \theta) 
      = & \Pr\left(\Pi_m(n) \geq 0 >
        \Pi_m(n+1) \right) \\
      = & \Pr\begin{pmatrix} S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n) - \delta(n) \geq \epsilon \\ \epsilon > S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n+1) - \delta(n+1)
      \end{pmatrix}    \\
      =
      & \includegraphics[width=0.3\textwidth,height=0.12\textwidth]{br-illustration} \\
      = & \Phi\left(S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n) - \delta(n) \right) - 
      \\ & - \Phi\left(S_m x_m^D \beta - x_m^C
        \gamma - S_m \alpha(n+1) - \delta(n+1)\right)
    \end{align*}
  }}
  \end{itemize}
\end{frame}
  
\begin{frame}\frametitle{Data}
  \begin{itemize}
  \item 202 isolated local markets 
    \begin{itemize}
    \item Population 500-75,000
    \item $\geq$ 20 miles from nearest town of 1,000$+$
    \item $\geq$ 100 miles from city of 100,000$+$
    \end{itemize}
  \item 16 industries: retail and professions, each estimated
    separately
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item For most industries, $\alpha(n)$ and $\delta(n)$ increase with
    $n$
  \item Define $S(n)=$ minimal $S$ such that $n$ firms enter
    \[ S(n) = \frac{x_m^C\gamma + \delta(n)}{x_m^D\beta - \alpha(n)} \]
    \begin{itemize}
    \item Varies across industries
    \end{itemize}
  \item $\frac{S(n)}{n} \approx$ constant for $n \geq 5$ 
    \begin{itemize}
    \item Contestable markets \citep{baumol1982} : an industry can be
      competitive even with few firms if there is easy entry
    \end{itemize}
  \end{itemize}  
\end{frame}

\begin{frame}
  \includegraphics[width=\textwidth]{br-thresholdTable}
\end{frame}

\begin{frame}
  \includegraphics[height=\textheight]{br-thresholdFigure}
\end{frame}


%\begin{frame}
% \frametitle{Example: restricting emissions of cement plants}
%  See Aguirregabiria (2012) Chapter 1, Section 3
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}
