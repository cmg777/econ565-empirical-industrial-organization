\input{../slideHeader}

\title{Estimating Production Functions}
\author{Paul Schrimpf}
\institute{UBC \\ Economics 565}
\date{\today}

\begin{document}

\frame{\titlepage}

\part{Estimating Production Functions}}


\begin{frame}
  \tableofcontents  
\end{frame}

\section{Introduction} 

\begin{frame}
  \frametitle{Why estimate production functions?}
  \begin{itemize}
  \item Primitive component of economic model 
  \item Gives estimate of firm productivity --- useful for
    understanding economic growth
    \begin{itemize}
    \item Stylized facts to inform theory, e.g. \cite{foster2001}
    \item Effect of deregulation, e.g. \cite{olley1996dynamics}
    \item Growth within old firms vs from entry of new firms,
      e.g. \cite{foster2006}
    \item Effect of trade liberalization, e.g. \cite{amiti2007}
    \item Effect of FDI \cite{javorcik2004}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{These slides based on:}
  \begin{itemize}
  \item \cite{aguirregabiria2012} chapter 2
  \item \cite{ackerberg2007econometric} section 2
  \item \cite{vanBeveren2012}
  \end{itemize}
\end{frame}  

\section{Setup}

\begin{frame}
  \frametitle{Setup}
  \begin{itemize}
  \item Cobb Douglas production
    \[ Y_{it} = A_{it} K_{it}^{\beta_k} L_{it}^{\beta_l} \]
  \item In logs,
    \[ y_{it} = \beta_k k_{it} + \beta_l l_{it} +
    \omega_{it} + \epsilon_{it} \]
    with $\log A_{it} = \omega_{it} + \epsilon_{it}$, $\omega_{it}$
    known to firm, $\epsilon_{it}$ not
  \item Problems: 
    \begin{enumerate}
    \item Simultaneity: if firm has information about $\log A_{it}$
      when choosing inputs, then inputs correlated with
      $\log A_{it}$, e.g.\ price $p$, wage $w$, perfect information
      \[ L_{it} = \left( \frac{p}{w} \beta_l A_{it} K_{it}^{\beta_k}\right)^{\frac{1}{1-\beta_l}}  \]
    \item Selection: firms with low productivity will exit sooner
    \item Others: measurement error, specification
    \end{enumerate}
  \end{itemize}
\end{frame}

\section{Simultaneity}

\begin{frame}
  \frametitle{Simultaneity solutions}
  \begin{enumerate}
  \item IV
  \item Panel data 
  \item Control functions
  \end{enumerate}
\end{frame}

\subsection{Instrumental variables}

\begin{frame} 
  \frametitle{Instrumental variables}
  \begin{itemize}
  \item Instrument must be
    \begin{itemize}
    \item Correlated with $k$ and $l$
    \item Uncorrelated with $\omega + \epsilon$
    \end{itemize}
  \item Possible instrument: input prices
    \begin{itemize}
    \item Correlated with $k$, $l$ through first-order condition
    \item Uncorrelated with $\omega$ if input market competitive
    \end{itemize}
  \item Other possible instruments: output prices (more often
    endogenous), input supply or output demand shifter (hard to find)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Problems with input prices as IV}
  \begin{itemize}
  \item Not available in some data sets
  \item Average input price of firm could reflect quality as well as
    price differences
  \item Need variation across observations
    \begin{itemize}
    \item If ﬁrms use homogeneous inputs, and operate in the same
      output and input markets, we should not expect to ﬁnd any
      signiﬁcant cross-sectional variation in input prices 
    \item If firms have different input markets, maybe variation in
      input prices, but different prices could be due to different
      average productivity across input markets
    \item Variation across time is potentially endogenous because
      could be driven by time series variation in average productivity  
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Panel data}

\subsubsection{Fixed effects}

\begin{frame} \frametitle{Fixed effects}
  \begin{itemize}
  \item Have panel data, so should consider fixed effects
  \item FE consistent if:
    \begin{enumerate}
    \item $\omega_{it} = \eta_i + \delta_t + \omega_{it}^\ast$ 
    \item $\omega_{it}^\ast$ uncorrelated with $l_{it}$ and $k_{it}$,
      e.g.\ $\omega_{it}^\ast$ only known to firm after choosing
      inputs
    \item $\omega_{it}^\ast$ not serially correlated and is strictly
      exogenous 
    \end{enumerate}
  \item Problems: 
    \begin{itemize}
    \item Fixed productivity a strong assumption
    \item Estimates often small in practice 
    \item Worsens measurement error problems
      \[ \mathrm{Bias}(\hat{\beta}^{FE}_k) \approx -\frac{\beta_k
        \var(\Delta \epsilon)} {\var(\Delta k) + \var(\Delta
        \epsilon)} \]
    \end{itemize}    
  \end{itemize}
\end{frame}

\subsubsection{Dynamic}
\begin{frame}[allowframebreaks]\frametitle{Dynamic panel: motivation} 
  \begin{itemize}
  \item General idea: relax fixed effects assumption, but still
    exploit panel    
  \item Collinearity problem: Cobb-Douglas production, flexible labor
    and capital implies log labor and log capital are linear functions
    of prices and productivity (\cite{bond2005})
  \item If observed labor and capital are not collinear then there
    must be something unobserved that varies across firms
    (e.g. prices), but that would invalidate monotonicity assumption
    of control function
  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Dynamic panel: moment conditions}
  \begin{itemize}
  \item See \cite{blundell2000}
  \item Assume $\omega_{it} = \gamma_t + \eta_i + \nu_{it}$ with
    $\nu_{it} = \rho \nu_{i,t-1} + e_{it}$, so
    \[ y_{it} = \beta_l l_{it} + \beta_k k_{it} + \gamma_t + \eta_i +
    \nu_{it} + \epsilon_{it} \]
    subtract $\rho y_{i,t-1}$ and rearrange to get
    \begin{align*}
      y_{it} = & \rho y_{i,t-1} + \beta_l ( l_{it} - \rho l_{i,t-1}) + \beta_k (k_{it} -
      \rho k_{i,t-1}) + \\ 
      & + \gamma_t - \rho \gamma_{t-1}
      + \underbrace{\eta_i(1-\rho)}_{=\eta_{i}^\ast} + \underbrace{e_{it} + \epsilon_{it} - \rho
        \epsilon_{i,t-1}}_{= w_{it}}
    \end{align*}
  \item Moment conditions: 
    \begin{itemize}
    \item Difference: $\Er[x_{i,t-s} \Delta w_{it}]=0$ where
      $x=(l,k,y)$
    \item Level: $\Er\left[ \Delta x_{i,t-s} (\eta_i^\ast +
        w_{it})\right] = 0$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks] 
  \frametitle{Dynamic panel: economic model}  
  \begin{itemize}
  \item Adjustment costs
    \begin{align*}
      V(K_{t-1},L_{t-1}) = \max_{I_t,K_t,H_t,L_t} & P_t F_t(K_t,L_t) - 
      P^K_t\left(I_t + G_t(I_t,K_{t-1}) \right) - \\ 
      & - W_t \left(L_t +
        C_t(H_t,L_{t-1}) \right) + \\
      \psi \Er\left[V(K_t,L_t) |
        \mathcal{I}_t\right]  \\
      \text{s.t. } & K_t = (1-\delta_k)K_{t-1} + I_t \\
      & L_t = (1-\delta_l) L_{t-1} + H_t 
    \end{align*}
    Implies
    \begin{align*}
      P_t \frac{\partial F_t}{\partial L_t} - W_t \frac{\partial
        C_t}{\partial L_t} = & W_t + \lambda_t^L\left(1 - (1-\delta_l)
        \psi \Er\left[\frac{\lambda^L_{t+1}}{\lambda_{t}^L} |
          \mathcal{I}_t \right]  \right) \\
      P_t \frac{\partial F_t}{\partial K_t} - P^K_t \frac{\partial
        G_t}{\partial K_t} = & \lambda_t^K\left(1 - (1-\delta_k)
        \psi \Er\left[\frac{\lambda^K_{t+1}}{\lambda_{t}^K} |
          \mathcal{I}_t \right]  \right)
    \end{align*}
    \begin{itemize}
    \item Current productivity shifts $\frac{\partial F_t}{\partial
        L_t}$ and (if correlated with future) the shadow value of
      future labor $\Er\left[\frac{\lambda^L_{t+1}}{\lambda_{t}^L} |
        \mathcal{I}_t \right]$
    \item Past labor correlated with current because of adjustment costs
    \end{itemize}    
  \end{itemize}
\end{frame}


\begin{frame}\frametitle{Dynamic panel data: problems} 
  \begin{itemize}
  \item Problems:
    \begin{itemize}
    \item Sometimes imprecise (especially if only use difference
      moment conditions)
    \item Differencing worsens measurement error
    \item Weak instrument issues if only use difference moment
      conditions but levels stronger (see \cite{blundell2000})
    \end{itemize}
  \end{itemize}
\end{frame}


\subsection{Control functions}

\begin{frame} \frametitle{Control functions}
  \begin{itemize}
  \item From \cite{olley1996dynamics} (OP)
  \item {\bfseries{Control function}}: function of data conditional on
    which endogeneity problem solved
    \begin{itemize}
    \item E.g. usual 2SLS $y = x\beta + \epsilon$, $x = z \pi + v$,
      control function is to estimate residual of reduced form,
      $\hat{v}$ and then regress $y$ on $x$ and $\hat{v}$. $\hat{v}$
      is the control function 
    \end{itemize}
  \item Main idea: model choice of inputs to find a control function 
  \end{itemize}
\end{frame}


\begin{frame}[shrink]\frametitle{OP assumptions}
  \[ y_{it} = \beta_k k_{it} + \beta_l l_{it} + \omega_{it} + \epsilon_{it} \]
  \begin{enumerate}
  \item $\omega_{it}$ follows exogenous first order Markov process, 
    \[ p(\omega_{it+1} | \mathcal{I}_{it}) = p(\omega_{it+1} |
    \omega_{it}) \]
    \note{Define first order Markov. \\
      Econometric assumption about evolution of $\omega$. \\
      Economic assumption about firm information.}      
  \item Capital at $t$ determined by investment at time $t-1$, 
    \[ k_t = (1-\delta) k_{it-1} + i_{it-1} \]
  \item Investment is a function of $\omega$ and other observed
    variables 
    \[
    i_{it} = I_t(k_{it}, \omega_{it}),
    \] 
    and is strictly increasing in $\omega_{it}$
    \note{
    \begin{itemize}
    \item $I_t()$ depends on $t$, so can have common unobserved
      input prices that change with time
    \item Unobserved variables that change across firms are not
      allowed to affect investment
    \end{itemize}
    }
  \item Labor variable and non-dynamic, i.e.\ chosen each $t$,
    current choice has no effect on future (can be relaxed)
  \end{enumerate}
\end{frame}

\begin{frame} 
  \frametitle{OP estimation of $\beta_l$}
  \begin{itemize}
  \item Invertible investment implies $\omega_{it} =
    I_t^{-1}(k_{it}, i_{it})$
    \begin{align*}
      y_{it} = & \beta_k k_{it} + \beta_l l_{it} + 
      I_t^{-1}(k_{it}, I_{it}) + \epsilon_{it} \\
      = & \beta_l l_{it} + f_t(k_{it}, i_{it}) + \epsilon_{it}
    \end{align*}  
  \item Partially linear model
    \begin{itemize}
    \item Estimate by e.g.\ regress $y_{it}$ on $l_{it}$ and series
      functions of $t, k_{it},i_{it}$
    \item Gives $\hat{\beta}_l$, $\hat{f}_{it} = \hat{f}_t(k_{it}, i_{it})$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame} 
  \frametitle{OP estimation of $\beta_k$}
  \begin{itemize}
  \item Note: $\hat{f}_t(k_{it}, i_{it}) = \hat{\omega}_{it} +
    \beta_k k_{it}$
  \item By assumptions, $ \omega_{it} = \Er[\omega_{it} |
    \omega_{it-1}] + \xi_{it} = g(\omega_{it-1}) + \xi_{it}$ with
    $\Er[\xi_{it} | k_{it} ] = 0$
  \item Use $\Er[\xi_{it} | k_{it} ] = 0$ as moment to estimate
    $\beta_k$.
    \begin{itemize}
    \item OP: write production function as
      \begin{align*}
        y_{it} - \beta_l l_{it} = & \beta_k k_{it} + 
        g(\omega_{it-1}) + \xi_{it} + \epsilon_{it} \\
        = &  \beta_k k_{it} +  
        g\left(f_{it-1} - \beta_k k_{it-1} \right) +
        \\
        & + \xi_{it} + \epsilon_{it} 
      \end{align*}
      Use $\hat{\beta}_l$ and $\hat{f}_{it}$ in equation above and
      estimate $\hat{\beta}_k$ by e.g.\ 
      semi-parametric nonlinear least squares
    \item \cite{ackerberg2006structural}: use $\Er \left[
        \hat{\xi}_{it}(\beta_k) k_{it} \right] = 0$  
    \end{itemize}
  \end{itemize}  
\end{frame}

\subsubsection{Critiques and extensions}

\begin{frame}
  \frametitle{Critiques and extensions}
  \begin{itemize}
  \item \cite{levinsohn2003}: investment often zero, so use other
    inputs instead of investment to form control function
  \item \cite{ackerberg2006structural}: control function often
    collinear with $l_{it}$ --- for it not to be must be firm specific
    unobervables affecting $l_{it}$ (but not investment / other input
    or else demand not invertible and cannot form control function)
  \item \cite{gandhi2009identifying}: relax scalar unobservable in
    investment / other input demand
  \item \cite{wooldridge2009}: more efficient joint estimation
  \item \cite{maican2006} and \cite{doraszelski2009}: endogenous
    productivity 
  \end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Selection}

\begin{frame} \frametitle{Selection}
  \begin{itemize}
  \item Let $d_{it} = 1 $ if firm in sample.
    \begin{itemize}
    \item Standard conditions imply $d = 1\{\omega
      \geq\omega^\ast(k)\}$   
    \end{itemize}
  \item Messes up moment conditions
    \begin{itemize}
    \item All estimators based on $\Er[ \omega_{it} \text{Something} ] =
      0$, observed data really use $\Er[ \omega_{it} \text{Something} |
      d_{it} = 1]$
    \item E.g.\ OLS okay if $\Er[ \omega_{it} |l_{it}, k_{it} ] = 0$,
      but even then,
      \begin{align*}
        \Er[ \omega_{it} | l_{it}, k_{it} , d_{it} = 1 ] = 
        & \Er[\omega_{it} | l_{it}, k_{it} , \omega_{it} \geq
        \omega^\ast( k_{it}) ] \\
        = & \lambda( k_{it} ) \neq 0
      \end{align*}
    \end{itemize}
  \item Selection bias negative, larger for capital than labor 
  \end{itemize}
\end{frame}

\subsection{OP and selection}

\begin{frame}
  \frametitle{Selection in OP model}
  \begin{itemize}
  \item Estimate $\beta_l$ as above
  \item Write $d_{it} = 1\{ \xi_{it} \leq \omega^\ast(k_{it})
    - \rho (f_{i,t-1} - \beta_k k_{it-1}) =
    h(k_{it},f_{it-1},k_{it-1}) \}$
  \item Propensity score $P_{it} \equiv \Er[d_{it} | k_{it},
    f_{it-1},k_{it-1}]$
  \item Similar to before estimate $\beta_k$, from
    \begin{align*}
      y_{it} - \beta_l l_{it} = & \beta_k k_{it} + 
      \tilde{g}\left(f_{it-1} - \beta_k k_{it-1},
        P_{it}\right) + \\ & + \xi_{it} + \epsilon_{it} 
    \end{align*}
  \end{itemize}
\end{frame}

\section{Applications}

\begin{frame}\frametitle{Applications}
  \begin{itemize}
  \item \cite{olley1996dynamics}: productivity in telecom after
    deregulation
  \item \cite{sth2006}: productivity and exit of African manufacturing
    firms, uses IV 
  \item \cite{levinsohn2003}: compare estimation methods using Chilean
    data
  \item \cite{javorcik2004}: FDI and productivity, uses OP
  \item \cite{amiti2007}: trade liberalization in Indonesia, uses OP
  \item \cite{aw2001}: productivity differentials and firm turnover in
    Taiwan
  \item \cite{kortum2000}: venture capital and innovation
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\part{Selected applications and extensions}

\frame{\partpage}

\begin{frame}
  \tableofcontents  
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{\cite{ackerberg2006structural}}

\begin{frame}\frametitle{\cite{ackerberg2006structural}: contributions}
  \begin{itemize}
  \item Document collinearity problem in OP and \citet{levinsohn2003}
    \begin{itemize}
    \item Need $l_{it}$, $f_{it}(k_{it}, i_{it})$ not collinear, i.e.\
      something causes variation in $l$, but not $k$
    \end{itemize}
  \item Propose alternative estimator
  \item Relates estimator to dynamic panel \citep{blundell2000}
    approach
  \item Illustrates estimator using Chilean data
  \end{itemize}
  \footnotetext{$^*$These slides are based on the working paper version \cite{ackerberg2006wp}.}
\end{frame}

\subsection{Collinearity in OP}

\begin{frame}[allowframebreaks]
  \frametitle{Collinearity in OP}
  \begin{itemize}
  \item OP assume $i_{it} = I_t(k_{it}, \omega_{it})$
  \item Symmetry, parsimony suggest $l_{it} = L_t(k_{it},
    \omega_{it})$ 
  \item Then $l_{it} = L_t(k_{it}, I_t^{-1}(k_{it}, i_{it}) ) =
    g_t(k_{it}, i_{it})$
    \[ y_{it} = \beta_l l_{it} + f_t(k_{it}, i_{it}) +
    \epsilon_{it} \]
    $l_{it}$ collinear with $f_t(k_{it}, i_{it})$
  \item Worse in \cite{levinsohn2003} 
    \begin{itemize}
    \item Uses other input $m_{it}$ to form control function
      \begin{align*}
        y_{it} = & \beta_l l_{it} + \beta_k k_{it} + \beta_m m_{it} +
        \omega_{it} + \epsilon_{it} \\
        m_{it} = & M_t(k_{it}, \omega_{it})
      \end{align*}
    \item Even less reason to treat labor demand differently than other
      input demand
    \end{itemize}
  \item Collinearity still problem with parametric input demand
  \item Plausible models that do not solve collinearity
    \begin{itemize}
    \item Input price data 
      \begin{itemize}
      \item Must include in control function to preserve scalar unobservable
      \item Same logic above implies $m$ and $l$ are functions of both prices,
        so still collinear
      \end{itemize}
    \item Adjustmest costs in labor
      \begin{itemize}
      \item Need to add $l_{it-1}$ to control function
      \end{itemize}
    \item Change in timing assumptions
    \item Measurement error in $l$ (but not $m$)
      \begin{itemize}
      \item Solves collinearity, but makes $\hat{\beta}_l$
        inconsistent
      \end{itemize}
    \end{itemize}
  \item Potential model change that removes collinearity
    \begin{itemize}
    \item Optimization error in $l$ (but not $m$)
    \item $m$ chosen, $l$ specific shock revealed, $l$ chosen
    \item OP only: $l_{it}$ chosen at $t-1/2$, $l_{it} =
      L_t(\omega_{it-1/2},k_{it})$, $i_{it}$ chosen at $t$
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{ACF estimator}
\begin{frame}[shrink]
  \frametitle{ACF estimator}
  \begin{itemize}
  \item Idea: like capital, labor is harder to adjust than other
    inputs 
  \item Model: $l_{it}$ chosen at time $t-1/2$, $m_{it}$ at time $t$
    \begin{itemize}
    \item Implies $m_t = M_t(k_{it},l_{it}, \omega_{it})$
    \end{itemize}
  \item Estimation:
    \begin{enumerate}
    \item $y_{it} = \underbrace{\beta_k k_{it} + \beta_l l_{it} +
        f_t(m_{it},k_{it},l_{it})}_{\equiv
        \Phi_t(m_{it},k_{it},l_{it})} + \epsilon_{it}$ gives
      \[ \hat{\omega}_{it}(\beta_k,\beta_l) = \hat{\Phi}_{it} -
      \beta_k k_{it} - \beta_l l_{it} \]
    \item Moments from timing and Markov process for $\omega_{it}$
      assumptions:
      \[ \omega_{it} = \Er[\omega_{it} | \omega_{it-1} ] + \xi_{it} \]
      \begin{itemize}
      \item $\Er[\xi_{it} | k_{it}]=0 $ as in OP
      \item $\Er[\xi_{it} | l_{it-1}] = 0$ from new timing assumption
      \item $\hat{\xi}_{it}(\beta_k,\beta_l)$ as residual from
        nonparametric regression of $\hat{\omega}_{it}$ on
        $\hat{\omega}_{it-1}$  
      \item Can add moments based on $\Er[\epsilon_{it} |
        \mathcal{I}_{it}] = 0$
      \end{itemize}
    \end{enumerate}
  \end{itemize}
\end{frame}

\subsection{Relation to dynamic panel}
\begin{frame}
  \frametitle{Relation to dynamic panel estimators}
  \begin{itemize}
  \item Both derive moment conditions from assumptions about timing
    and information set of firm
  \item Dealing with $\omega$
    \begin{itemize}
    \item Dynamic panel: AR(1) assumption allows quasi-differencing
    \item Control function: makes $\omega$ estimable function of
      observables
    \end{itemize}
  \item Dynamic panel allows fixed effects, does not make assumptions
    about input demand
  \item Control function allows more flexible process for $\omega_{it}$
  \end{itemize}
\end{frame}

\subsection{Empirical example}
\begin{frame}
  \frametitle{Empirical example}
  \begin{itemize}
  \item Chilean plant level data
  \item Compare OLS, FE, LP, ACF, and dynamic panel estimators
  \item LP and ACF using three different inputs (materials,
    electricity, fuel) for control function
  \item Results: 
    \begin{itemize}
    \item 311=food, 321=textiles, 331=wood, 381=metal
    \item Expected biases in OLS and FE
    \item ACF and LP significantly different
    \item ACF less sensitive to which input used for control function
    \item Dynamic panel closer to ACF than LP, but still significant
      differences      
    \end{itemize}
  \end{itemize}  
\end{frame}

\frame[plain]{\includegraphics[page=39,width=\textwidth]{ACF20withtables}}
%\frame{\includegraphics[page=40,height=\textheight]{ACF20withtables}}
\frame[plain]{\includegraphics[page=40,width=\textwidth]{ACF20withtables}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{\cite{gandhi2009identifying}}

\begin{frame}\frametitle{\cite{gandhi2009identifying}}
  \begin{itemize}
  \item Show that control function method is not nonparametrically 
    identified when there are flexible inputs 
  \item Propose alternate estimate that uses data on input shares and
    information from firm's first order condtiion
  \item Show that value-added and gross output production functions
    are incompatible
  \item Application to Colombia and Chile 
  \end{itemize}
\end{frame}  

\subsection{Identification problem}

\begin{frame}\frametitle{Assumptions} 
  \begin{enumerate}
  \item Hicks neutral productivity $Y_{jt} = e^{\omega_{jt} +
      \epsilon_{jt}} F_t(L_{jt},K_{jt},M_{jt}) $
  \item $\omega_{jt}$ Markov, $\epsilon_{jt}$ i.i.d.
  \item $K_{jt}$ and $L_{jt}$ determined at $t-1$, $M_{jt}$ determined
    flexibly at $t$
    \begin{itemize}
    \item $K$ and $L$ play same role in the model, so after this slide
      I will drop $L$ 
    \end{itemize}
  \item $M_{jt} = \mathbb{M}_t(L_{jt}, K_{jt}, \omega_{jt})$, monotone
    in $\omega_{jt}$
  \end{enumerate}
\end{frame}

\begin{frame} \frametitle{Reduced form}
  \begin{itemize}
  \item Let $h(\omega_{jt-1}) = \Er[\omega_{jt}|\omega_{jt-1}]$,
    $\eta_{jt} = \omega_{jt} - h(\omega_{jt-1})$
  \item log output
    \begin{align*} 
      y_{jt} = & f_t(k_{jt},m_{jt}) + \omega_{jt} + \epsilon_{jt} \\
      = & f_t(k_{jt},m_{jt}) + 
      \underbrace{h(\mathbb{M}_{t-1}^{-1}(k_{jt-1},m_{jt-1}))}_{=h_{t-1}(k_{jt-1},m_{jt-1})}
      + \eta_{jt} + \epsilon_{jt}
    \end{align*}
  \item Assumptions imply
    \[ \Er[\eta_{jt} | \underbrace{k_{jt}, k_{jt-1}, m_{jt-1}, ... k_{j1}, m_{j1}}_{=\Gamma_{jt}}]
    = 0 \]
  \item Reduced form 
    \begin{align}
      \Er[y_{jt} |\Gamma_{jt}] = & \Er[f_t(k_{jt}, m_{jt}) |
      \Gamma_{jt}] +  h_{t-1}(k_{jt-1},m_{jt-1}) \label{eq:id}
    \end{align}
  \item Identification: given observed $ \Er[y_{jt} |\Gamma_{jt}] $ is
    there a unique $f_t$, $h_{t-1}$ that satisfies (\ref{eq:id})?
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Example: Cobb-Douglas}
  \begin{itemize}
  \item Let $f_t(k,m) = \beta_k k + \beta_m m$
  \item Assume firm is takes prices as given
  \item First order condition for $m$ gives
    \[ m = constant + \frac{\beta_k}{1-\beta_m} k +
    \frac{1}{1-\beta_m} \omega \]
  \item Put into reduced form 
    \begin{align}
      \Er[y_{jt} |\Gamma_{jt}] = & C + \frac{\beta_k}{1-\beta_m}
      k_{jt} + \frac{\beta_m}{1-\beta_m} \Er[\omega_{jt} | \Gamma_{jt}] +
      h_{t-1}(k_{jt-1},m_{jt-1}) \label{eq:id} 
    \end{align}
  \item $\omega$ Markov and $\omega_{jt-1} =
    \mathbb{M}^{-1}_{t-1}(k_{jt-1},m_{jt-1})$ implies
    \begin{align*}
      \Er[\omega_{jt} | \Gamma_{jt}] = & \Er[\omega_{jt} |
      \omega_{jt-1} = \mathbb{M}^{-1}_{t-1}(k_{jt-1},m_{jt-1})] = \\
      = &  h_{t-1}(k_{jt-1},m_{jt-1}) 
    \end{align*}
  \item Which leaves
    \begin{align}
      \Er[y_{jt} |\Gamma_{jt}] = & constant + \frac{\beta_k}{1-\beta_m}
      k_{jt} + \frac{1}{1-\beta_m} h_{t-1}(k_{jt-1},m_{jt-1}) \label{eq:id} 
    \end{align}
    from which $\beta_k$, $\beta_m$ are not identified
  \item Rank condition fails, $\Er[m_{jt} | \Gamma_{jt}]$ is colinear
    with $h_{t-1}(k_{jt-1}, m_{jt-1})$
  \item After conditioning on $k_{jt}, k_{jt-1}, m_{jt-1}$, only
    variation in $m_{jt}$ is from $\eta_{jt}$, but this is
    uncorrelated with the instruments
  \end{itemize}
\end{frame}

% \begin{frame}[allowframebreaks]
%   \frametitle{Proof of non-identification}
%   \begin{itemize}
%   \item I do not doubt the claim of non-identification, but I do not
%     understand the proof in the paper
%   \item Lemma 2 incorrect? 
%   \item Alternate proof 
%   \end{itemize}
% \end{frame}

\subsection{Identification from first order conditions}

\begin{frame}[allowframebreaks]
  \frametitle{Identification from first order conditions}
  \begin{itemize}
  \item Since $m$ flexible, it satisfies a simple static first order
    condition, 
    \begin{align*}
      \rho_t = & p_t \frac{\partial F_t}{\partial M}
      \Er[e^{\epsilon_{jt}}] e^{\omega_{jt}} \\
      \log \rho_t = & \log p_t + \log \frac{\partial F_t}{\partial
        M}(k_{jt},m_{jt}) + \log \Er[e^{\epsilon_{jt}}] + \omega_{jt}
    \end{align*}
  \item Problem: prices often unobserved, endogenous $\omega$
  \item Solution: difference from output equation to eliminate
    $\omega$, rearrange so that it involves only the value of materials
    and the value of output (which are often observed)
    \begin{align*}
      \underbrace{s_{jt}}_{\equiv \log \frac{\rho_t M_{jt}}{p_t Y_{jt}}} = & \log \underbrace{G_t(k_{jt},
      m_{jt})}_{\equiv \left(M_t \frac{\partial F_t}{\partial
          M}\right)/F_t} + \log \underbrace{\Er[e^{\epsilon_{jt}}]}_{\mathcal{E}} - \epsilon_{jt} 
    \end{align*}
  \item Identifies elasticity up to scale, $G_t \mathcal{E}$ and
    $\epsilon_{jt}$ which identifie $\mathcal{E}$
  \item Integrating, 
    \[ \int_{m_0}^{m_{jt}} G_t(k_{jt},m)/m = 
    f_t(k_{jt},m_{jt}) + c_t(k_{jt}) \]
    identifies $f$ up to location 
  \item Output equation
    \begin{align*}
      y_{jt} = & \int_{m_0}^{m_{jt}}
      \tilde{G}_t(k_{jt},m)/m - c_t(k_{jt}) + \omega_{jt} + \epsilon_{jt} \\
      -c_t(k_{jt}) +
      \omega_{jt} = & \underbrace{y_{jt} - \int_{m_0}^{m_{jt}} \tilde{G}_t(k_{jt},m)/m
        - \epsilon_{jt}}_{\equiv \mathcal{Y}_{jt}} 
    \end{align*}
    where the things on the right have already been identified
  \item Identify $c_t$ from
    \begin{align*}
      \mathcal{Y}_{jt}= & -c_t(k_{jt}) +
      \tilde{h}_t( \mathcal{Y}_{jt-1},k_{jt-1}) + \eta_{jt}
    \end{align*}
  \end{itemize}
\end{frame}

\subsection{Value added vs gross production}

\begin{frame}\frametitle{Value added vs gross production}
  \begin{itemize}
  \item Value added:
    \begin{align*}
      VA_{jt} = & p_t Y_{jt} - \rho_t M_{jt} \\
      = & p_t F_t(K_{jt},
      \mathbb{M}_t(K_{jt},\omega_{jt}))e^{\omega_{jt}+\epsilon_{jt}} - 
      \rho_t \mathbb{M}_t(K_{jt},\omega_{jt}) 
  \end{align*}
\item Envelope theorem implies $\text{elasticity}_{e^\omega}^Y \approx
    \text{elasticity}_{e^\omega}^{VA} (1 -  \frac{\rho_t M_{jt}}{p_t
      Y_{jt}})$
  \end{itemize}
  Problems
  \begin{itemize}
  \item Production Hicks-neutral productivity does not imply
    value-added Hicks-neutral productivity
  \item Ex-post shocks $\epsilon_{jt}$ not accounted for
    in approximation
  \end{itemize}
\end{frame}

\subsection{Empirical results}
\begin{frame}\frametitle{Empirical results}
  \begin{itemize}
  \item Look at tables
  \item Value-added estimates imply much more productivity dispersion
    than gross (90-10) ratio of 4 vs 2
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
\section{\cite{grieco2017}}

\begin{frame}\frametitle{\cite{grieco2017}}
  \url{https://www.ftc.gov/sites/default/files/documents/public_events/fifth-annual-microeconomics-conference/grieco-p_0.pdf}
\end{frame}

\begin{frame}[allowframebreaks]\frametitle{My thoughts while reading}
  \begin{itemize}
  \item How they got the data    
  \item Incentive shifters may be correlated with quality and
    productivity
    \begin{itemize}
    \item Time since inspection 
      \begin{itemize}
      \item[+] Inspect recently $\Rightarrow$ higher quality incentive
      \item[-] Low productivity $\Rightarrow$ low quality
        $\Rightarrow$ more inspections
      \end{itemize}
    \item Referral rate 
    \item Section 5.2: incentive shifters can be correlated with
      productivity, only need that shape of production possibilities
      frontier is invariant
    \end{itemize}    
  \item Should hemoglobin level be controlled for when measuring
    quality?
    \begin{itemize}
    \item Anemia (low hemoglobin) is risk-factor for infection
    \item Anemia can be treated through diet, iron supplements (pills
      or IV), EPO, etc
      \begin{itemize}
      \item Are dialysis facilities responsible for this treatment? 
      \item In 2006-2014 data average full-time dieticiens = 0.5,
        average part-time = 0.6
      \end{itemize}      
    \end{itemize}
  \item Estimation details:
    \begin{itemize}
    \item[Step 1:]   Estimate $\alpha_q$
      \[ y_{jt} − \hat{E}[y|h_{jt} , i_{jt} , k_{jt} , \ell_{jt} ,
        x_{jt} ] = −\alpha_q (q jt − \hat{E}[q|h_{jt} , i_{jt} ,
        k_{jt} , \ell_{jt} , x_{jt} ]) + \epsilon_{jt} \]
      \begin{itemize}
      \item Drop observations with $h_{jt} = 0$ (not invertibility) 
      \item Okay here, because selecting on $\omega$, and residual,
        $\epsilon_{jt}$ is uncorrelated with $\omega$
      \item Problematic in last step? No, see footnote 49
      \end{itemize}
    \item[Step 2:]  Estimate $\beta_k$, $\beta_\ell$ from
      \[ y_{jt} + \hat{\alpha_q } + \beta_k k_{jt} + \beta_\ell
        \ell_{jt} = g(\hat{\omega}_{jt-1}(\beta)) + \eta_{jt} +
        \epsilon_{jt} \]
      \begin{itemize} 
      \item Only have $hat{\omega}_{jt-1}(\beta)$ when
        $h_{jt-1}\neq 0$, okay because $\epsilon_{jt}$ and $\eta_{jt}$
        are uncorrelated with $\omega_{jt-1}$, would be problem if
        using $\hat{\omega}_{jt}$ 
      \end{itemize}
    \item Nothing about selection --- number of centers, 4270, vs
      center-years, 18295, implies there must be entry and exit
    \end{itemize}  
  \item Would like to see some results related to productivity
    dispersion e.g.\
    \begin{itemize}
    \item Decompose variation in infection rate into: productivity
      variation, incentive variation, quality-quantity choices, and
      random shocks
    \item Compare strengthening incentives vs closing least productive
      facilities as policies to increase quality
    \end{itemize}
  \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{amiti2007}}

\begin{frame}
  \frametitle{Overview}
  \begin{itemize}
  \item Effect of reducing input and output tariffs on productivity
  \item Reducing output tariffs affects productivity by increasing competition
  \item Reducing input tariffs affects productivity through learning,
    variety, and quality effects
  \item Previous empirical work focused on output tariffs; might be
    estimating combined effect 
  \item Input tariffs hard to measure; with Indonesian data on
    plant-level inputs can construct plant specific input tariff  
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Methodology}
  \begin{itemize}
  \item Estimate TFP using Olley-Pakes
    \begin{itemize}
    \item Output measure is revenue $\Rightarrow$ may confound
      productivity and markups
    \end{itemize}
  \item Estimate relation between TFP and tariffs
    \begin{align}
      \log(TFP_{it}) = & \gamma_0 + \alpha_i + \alpha_{tl(i)} + \gamma_1
      (\text{output tariff})_{tk(i)} + \notag \\ & + \gamma_2 (\text{input
        tariff})_{tk(i)} + \epsilon_{it} \label{tfpeq}
    \end{align}
    \begin{itemize}
    \item $k(i)=$
      \href{http://unstats.un.org/unsd/cr/registry/regcst.asp?Cl=27&Lg=1}{5-digit
        (ISIC)} industry of plant $i$ 
    \item $l(i)=$ island of plant $i$
    \end{itemize}
  \item Explore robustness to:
    \begin{itemize}
    \item Different productivity measure
    \item Specification of \ref{tfpeq}
    \item Endogeneity of tariffs
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Data and tariff measure}
  \begin{itemize}
  \item Indonesian annual manufacturing census of $20+$ employee
    plants 1991-2001, after cleaning $~15,000$ firms per year
  \item Input tariffs:
    \begin{itemize}
    \item Data on tariffs on goods, $\tau_{jt}$, but also need to know inputs 
    \item 1998 only: have data on inputs, use to construct input
      weights at industry level, $w_{jk}$
    \item Industry input tariff $= \sum_{j} w_{jk} \tau_{jt}$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{itemize}
  \item Look at tables
  \item Input tariffs have larger effect than output, $\hat{\gamma}_1
    \approx -0.07$, $\hat{\gamma}_2 \approx -0.44$
  \item Robust to:
    \begin{itemize}
    \item Productivity measure
    \item Tariff measure
    \item Including/excluding Asian financial crisis
    \end{itemize}
  \item Less robust to instrumenting for tariffs
    \begin{itemize}
    \item Qualitatively similar, but larger coefficient estimates
    \end{itemize}
  \item Explore channels for productivity change
    \begin{itemize}
    \item Markups (maybe), product switching/addition (no), foreign 
      ownership (no), exporters (no)
    \end{itemize}
  \end{itemize}
\end{frame}

% \begin{frame} \frametitle{Criticism}  
%   \begin{itemize}
%   \item Methodology:
%     \begin{itemize}
%     %\item Could use LP, ACF, or dynamic panel methods to estimate
%       TFP
%     \item Standard errors in productivity regression appear to ignore
%       uncertainty from estimating TFP
%     \item Measurement error in tariffs could be taken more seriously
%       (is this why IV estimates are larger?)
%     \end{itemize}
%   \item 
%   \end{itemize}
% \end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\cite{doraszelski2009}}
\begin{frame}
  \frametitle{Overview}
  \begin{itemize}
  \item Estimable model of endogenous productivity, which combines:
    \begin{itemize}
    \item Knowledge capital model of R\&D
    \item OP \& LP productivity estimation
    \end{itemize}
  \item Application to Spanish manufacturers focusing on R\&D
    \begin{itemize}
    \item Large uncertainty (20\%-60\% or productivity
      unpredictable )
    \item Complementarities and increasing returns
    \item Return to R\&D larger than return to physical capital
      investment 
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle{Model (simplified)}
  \begin{itemize}
  \item Cobb-Douglas production:
    \[ 
    y_{it} = \beta_l l_{it} + \beta_k k_{it} +  \omega_{it} + \epsilon_{it} 
    \]
  \item Controlled Markov process for productivity, $p(\omega_{it+1} |
    \omega_{it}, r_{it})$, 
    \[ \omega_{it} = g(\omega_{it-1}, r_{it-1}) + \xi_{it} \]
  \item Labor flexible and non-dynamic
  \item Value function
    \begin{align*}
      V(k_t, \omega_t, u_t) = \max_{i,r} & \Pi(k_t,\omega_t) - C_i(i,u_t) -
      C_r(r,u_t) + \\ & +  \frac{1}{1+\rho} \Er\left[ V(k_{t+1}, \omega_{t+1},u_{t+1}) |
        k_t, \omega_t, i, r,  u_t \right] 
    \end{align*}
    \begin{itemize}
    \item $u$ scalar or vector valued shock
    \item $u$ not explicitly part of model, but identification
      discussion (especially p10 and footnote 6) implicitly adds it  
    \item $u$ independent of? $k$, $l$? across time?
    \end{itemize}
  \item Control function incorporating Cobb-Douglas assumption (and
    perfect competition):
    \[ \omega_{it} = h(l_{it}, k_{it}, w_{it} - p_{it}; \beta) =
    \lambda_0 + (1-\beta_l) l_{it} - \beta_k k_{it} + (w_{it} -
    p_{it}) \]
  \item Form moments based on
    \[ 
    y_{it} = \beta_l l_{it} + \beta_k k_{it} + g\left(h(l_{it-1}, k_{it-1},
      w_{it-1} - p_{it-1}; \beta), r_{it-1} \right) + \xi_{it} + \epsilon_{it}
    \epsilon_{it} 
    \] 
  \item No collinearity because:
    \begin{itemize}
    \item Parametric $h$
    \item Variation in $k$, $r$ due to $u$
    \end{itemize}
  \item Estimated model adds
    \begin{itemize}
    \item Material input instead of labor for control function
    \item $h$ based on imperfect competition
    \end{itemize}
  \item Comparison to OP, LP, ACF
  \end{itemize}
\end{frame} 


\begin{frame}\frametitle{Results}
  \begin{itemize}
  \item Look at tables and figures
  \item Large uncertainty (20\%-60\% or productivity
    unpredictable )
  \item Complementarities and increasing returns
  \item Return to R\&D larger than return to physical capital    
  \end{itemize}
\end{frame}

% \begin{frame} \frametitle{Criticism}  
%   \begin{itemize}
%   \item 
%     \begin{itemize}
%     \item Methodological: describe estimator, discuss relation to OP,
%       LP, ACF, 
%     \item Empirical: R\& D and uncertainty
%     \end{itemize}
%   \item Figures, elasticity estimates, and other results that are
%     function of parameter estimates do not have standard errors (very
%     common but bad)
%   \item
%   \end{itemize} 
% \end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[allowframebreaks]
\bibliographystyle{jpe}
\bibliography{../565}
\end{frame}

\end{document}