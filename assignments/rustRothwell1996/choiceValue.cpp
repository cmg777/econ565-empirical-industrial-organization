#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector choiceValue(NumericMatrix u,
                          IntegerMatrix feasible,                          
                          double discount,
                          NumericVector p,
                          int T) 
{
  int S = u.nrow();
  int A = u.ncol();  
  NumericVector v(Dimension(T,S,A));
  NumericVector logSumExpVp1(S);
  int t = T-1;
  for (int s=0;s<S;s++) {
    logSumExpVp1[s] = 0;
    for (int a=0;a<A;a++) {
      v[t + s*T  + a*S*T] = u(s,a);
      if (feasible(s,a)) logSumExpVp1[s] += exp(v[t + s*T  + a*S*T]);
    }
    logSumExpVp1[s] = log(logSumExpVp1[s]);
  }
  
  for (t = (T-2); t>=0; t--) {
    for (int s=0;s<S;s++) {
      for (int a=0;a<A;a++) {
        double ev = 0;
        for (int s2=0;s2<S;s2++) ev += logSumExpVp1[s2]* 
                                       p[s2 + s*S + a*S*S];
        v[t + s*T  + a*S*T] = u(s,a) + discount*ev;
      } 
    }
    for (int s=0;s<S;s++) {
      logSumExpVp1[s] = 0;
      for (int a=0;a<A;a++) {
        if (feasible(s,a)) logSumExpVp1[s] += exp(v[t + s*T + a*S*T]);
      }
      logSumExpVp1[s] = log(logSumExpVp1[s]);
    }
  }
  
  return(v);
}
