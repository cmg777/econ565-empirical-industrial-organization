\documentclass[11pt,reqno,letter]{amsart}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{graphicx}
%\usepackage{epstopdf}
\usepackage{hyperref}
\hypersetup{colorlinks=true}
\usepackage[left=1in,right=1in,top=0.9in,bottom=0.9in]{geometry}
\usepackage{multirow}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{mdframed}
%\usepackage[small,compact]{titlesec}
\usepackage{listings}

\usepackage{natbib}
\renewcommand{\cite}{\citet}

%\usepackage{pxfonts}
%\usepackage{isomath}
\usepackage{mathpazo}
%\usepackage{arev} %     (Arev/Vera Sans)
%\usepackage{eulervm} %_   (Euler Math)
%\usepackage{fixmath} %  (Computer Modern)
%\usepackage{hvmath} %_   (HV-Math/Helvetica)
%\usepackage{tmmath} %_   (TM-Math/Times)
%\usepackage{cmbright}
%\usepackage{ccfonts} \usepackage[T1]{fontenc}
%\usepackage[garamond]{mathdesign}
\usepackage{color}
\usepackage[normalem]{ulem}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{conjecture}{Conjecture}[section]
\newtheorem{corollary}{Corollary}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{proposition}{Proposition}[section]
\theoremstyle{definition}
\newtheorem{assumption}{}[section]
%\renewcommand{\theassumption}{C\arabic{assumption}}
\newtheorem{definition}{Definition}[section]
\newtheorem{step}{Step}[section]
\newtheorem{remark}{Comment}[section]
\newtheorem{example}{Example}[section]
\newtheorem*{example*}{Example}

\linespread{1.0}

\pagestyle{fancy}
%\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\fancyhead{}
\fancyfoot{}
%\fancyhead[LE,LO]{\tiny{\thepage}}
\fancyhead[CE,CO]{\tiny{\rightmark}}
\fancyfoot[C]{\small{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\newtheoremstyle{problem}% name
{12pt}% Space above
{5pt}% Space below
{}% Body font
{}% Indent amount
{\bfseries}% Theorem head font
{:}% Punctuation after theorem head
{.5em}% Space after theorem head
{}% Theorem head spec (can be left empty, meaning `normal')

\theoremstyle{problem}
\newtheorem{problem}{Problem}

\newtheoremstyle{solution}% name
{2pt}% Space above
{12pt}% Space below
{}% Body font
{}% Indent amount
{\bfseries}% Theorem head font
{:}% Punctuation after theorem head
{.5em}% Space after theorem head
{}% Theorem head spec (can be left empty, meaning `normal')
\newtheorem{soln}{Solution}

\newenvironment{solution}
  {\begin{mdframed}\begin{soln}$\,$}
  {\end{soln}\end{mdframed}}


\lstset{language=R}
\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\lstset{keywordstyle=\color[rgb]{0,0,1},                                        % keywords
        commentstyle=\color[rgb]{0.133,0.545,0.133},    % comments
        stringstyle=\color[rgb]{0.627,0.126,0.941}      % strings
}       
\lstset{
  showstringspaces=false,       % not emphasize spaces in strings 
  %columns=fixed,
  %numbersep=3mm, numbers=left, numberstyle=\tiny,       % number style
  frame=1mm,
  framexleftmargin=6mm, xleftmargin=6mm         % tweak margins
}


\title{Dynamic decision modeling: replication of Rust and Rothwell
  (1995)}
\date{Due: November 18th, 2015}
\author{Paul Schrimpf}

\begin{document}
\maketitle

This assignment will attempt to replicate the result of
\citet{rr1995}. 

\begin{problem}
  Start by reading \citet{rr1995}. You may also want to read the
  very related \citet{rr1995w}. There is nothing to hand in for this
  question. 
\end{problem}

\begin{problem}[Explore the data]
  The file \href{https://bitbucket.org/paulschrimpf/econ565/src/master/assignments/rustRothwell1996/loadRRdata.R}{loadRRdata.R} downloads Rust and
  Rothwell's data, loads it into R, and creates the state and action
  variables described in the paper. To check that the data is correct,
  reproduce Table I and Figures 3, 4, and 7. If you'd like you could
  also try to recreate some of the other figures from either
  \citet{rr1995} or \citet{rr1995w}. 

  There are many ways to create Table I. You could use the data.table
  package. You could also use the ``aggregate'' function. For example,
  to create the first three rows:
\begin{lstlisting}
df <- plantData
df$percent.time.operating <- df$hrs.operating/df$hrs.total
df$percent.time.refueling <- df$hrs.refuel/df$hrs.total
df$percent.time.forced.out <- df$hrs.forced.out/df$hrs.total
df$percent.time.planned.out <- df$hrs.plan.out/df$hrs.total
df$era <- "1975-79"
df$era[df$year>=80] <- "1980-83"
df$era[df$year>=84] <- "1984-93"
tab1 <- aggregate(. ~ era, df[,c("era", "percent.time.operating",
                         "percent.time.refueling", "percent.time.forced.out",
                         "percent.time.planned.out")], FUN=mean)
\end{lstlisting}
  
  I suggest using ggplot2 to create the figures. For example, for
  Figure 3, 
\begin{lstlisting}
fig3 <- ggplot(data=aggregate(. ~ year, plantData, mean),
               aes(x=year,y=num.forced.out)) + geom_line()
\end{lstlisting}
  
  If the results do not match the paper (and they do not exactly
  match), comment on whether you think the differences are
  important. You could also double check the loadRRdata.R code for
  whether all the variables are being defined correctly. I did not
  make any deliberate mistakes, but I may have overlooked something.
\end{problem}

\begin{problem}[Transition probabilities]
  The first step in estimating the dynamic model is to estimate the
  state transition probabilities, which are denoted by
  $p(x'|x,a,\psi)$ in the paper. The details of how this is done are
  described more clearly in \citet{rr1995w} than in
  \citet{rr1995}. The transitions of duration ($d_t$ in their paper,
  \texttt{duration} in my code) and spell type ($r_t$ in their paper,
  \texttt{spelltype} in my code) are deterministic. The only thing
  that is stochastic is the signal received by the power plant
  operator ($f_t$ in their paper, \texttt{npp.signal} in my code). If
  a plant is currently refueling, it can only go back to operating if
  it does not receive a ``continue refueling'' signal ($f_t = 2$ or
  \texttt{npp.signal=cont.refuel}). This happens with probability
  $p_{ro}(x)$. In the paper they allow $p_{ro}$ to depend on the
  duration of the current refueling spell and the age of the power
  plant. We will ignore age to simplify. The timing of decisions is as
  follows:
  \begin{itemize}
  \item Period begins state is $(r_t, d_t)$. The signal, $f_t$ and
    shocks $\epsilon_t$ are observed  
  \item Action, $a_t$, is chosen based on signal, shocks, and state
  \item Next state, $(r_{t+1}, d_{t+1})$, is determined based on action
  \item Proceed to time $t+1$
  \end{itemize}
  loadRRdata.R follows this timing convention. 
  
  Forced outage signals can occur next period during either spell
  type, unless the current signal is ``continue refueling'' and the
  current spell is ``refueling.'' We will estimate the probability of
  a forced outage signal given the duration of the current spell using
  a logit. A puzzling thing about the data is that there are a few very
  long spells. The size of the state space is proportional to the
  length of the longest spell. To keep the state space manageable, I
  arbitrarily censored spell lengths at 36 months. It is unclear what
  \cite{rr1995} did. It may be that I have made an error when creating
  the duration variable, but I cannot find any. 
\begin{lstlisting}
## censor durations at 36 months (unclear what R&R did. Also unclear
## why we have some very long durations.)
plantData$dur.cens <- plantData$duration
max.duration <- 36
plantData$dur.cens[plantData$dur.cens>max.duration] <- max.duration
\end{lstlisting}
  The R command for estimating a logit is glm, which stands for
  generalized linear model. The following code estimates the
  probability of a forced signal and then plots the results.
\begin{lstlisting}
# estimate P(forced outage | duration)
pof.glm <- glm( (npp.signal=="forced.outage") ~ as.factor(dur.cens),
               family=binomial(link=logit) ,
               data=subset(plantData,npp.signal!="cont.refuel"))
# plot estimated probability along with 95% confidence bands
md <- max(subset(plantData,npp.signal!="cont.refuel")$dur.cens)
df <- data.frame(dur.cens=1:md)
tmp <- predict(pof.glm,newdata=df,type="response", se.fit=TRUE)
df$p <- tmp$fit
df$se.p <- tmp$se.fit
df$lo <- df$p - df$se.p*1.96
df$hi <- df$p + df$se.p*1.96
fig10.of <- ggplot(data=df, aes(x=dur.cens, y=p)) +
  geom_line() + geom_line(aes(y=lo),linetype="dashed") +
  geom_line(aes(y=hi),linetype="dashed")
rm(df,tmp)
\end{lstlisting}
  This code allows $p_{of}(d)$ to be a completely flexible function of
  $d$ (the \texttt{as.factor(dur.cens)} puts in dummies for each
  duraction length). Based on \cite{rr1995w}, it looks like Rust and Rothwell
  specified $p_{of}(d)$ to be a quadratic function of $d$. You can
  choose to specify $p_{of}(d)$ however you think is best. 

  \begin{enumerate}
  \item Similarly estimate $p_{ro}(d)$. For this you will only want to
    use the observation when the spell type is "refueling" and the
    outcome is getting a signal other than "cont.refuel."
  \end{enumerate}
\end{problem}

\begin{problem}[Calculating the likelihood]
  Equations (4) and (5) of \cite{rr1995} are the key ingredients in
  the likelihood. Equation (4) gives the conditional choice
  probabilities implied by the model given the choice-specific value
  function. Equation (5) defines the choice specific value
  function. Equation (5) is
  \begin{align}
    v_t(x,a) = u(x,a,\phi) + \beta \int \log \left[ \sum_{a' \in
        A(x')} \exp(v_{t+1}(x',a')) \right]  p(dx'|x,a,\psi)
  \end{align}
  To calculate this we need to:
  \begin{itemize}
  \item Find the flow utility given a state, $x$, actions $a$, and
    parameters $\phi$.
  \item Take the sum over feasible actions in a given state to
    calculate $\sum_{a' \in
      A(x')}$ 
  \item Take the sum over states that are reachable given action $a$
    and state $x$ to calculate the integral
  \item Use our estimates of $p_{ro}(d)$ and $p_{of}(d)$ to calculate
    $p(dx'|x,a,\psi)$ 
  \end{itemize}
  There are various ways to accomplish this. Let there be $n_x$ states
  and $n_a$ actions. $u(\cdot,\cdot,\phi)$ and $v_t(\cdot,
  \cdot)$ can be represented by an $n_x \times n_a$ matrix; and
  $p(\cdot|\cdot,a)$ is an $n_x \times n_x$ matrix for each $a$. With
  this setup, the following code calculates $v$ given $u$ and $p$.
\begin{lstlisting}
## Calculate choice specific value function
## v(t,x,a) =  u(x,a) + beta*E[max_{a'} v(t+1,x',a') + e(a') | x, a]
choice.value <- function( u, discount, p.x, T) {
  v <- array(dim=c(T,nrow(u),ncol(u)))
  v[T,,] <- u[,]
  vtp1 <- v[T,,]
  for(t in (T-1):1) {
    for (a in 1:ncol(u)) {
      v[t,,a] <- u[,a] + discount*log( rowSums(exp(vtp1)*feasible.action) ) %*% p.x[,,a]
    }
    vtp1 <- v[t,,]
  }
  return(v)
}
\end{lstlisting}
  where \texttt{feasible.action} is a $n_x \times n_a$ matrix with
  $i,j$th entry equal to $1$ action $j$ is feasible in state $i$ and
  $0$ otherwise. The only time some actions are infeasible is when
  there is a ``continue refueling'' signal. 

  To create \texttt{u}, \texttt{p.x}, and \texttt{feasible.action}, we
  to assign each state vector and combination of actions a scalar
  index. Actions are already a scalar, so it's easy for them, but
  states are a vector so some works is needed. The following code
  implements a mapping from state vector to indices and vice-versa.
\begin{lstlisting}
# Given vector of variables that define a state, create a function
# that returns an index for each unique combination of them, and a
# function that given an index returns a state vector.
# The two resulting functions are inverses of one another
vector.index.converter <- function(data, state.vars) {
  nv <- rep(NA,length(state.vars))
  state.levels <- list()
  for (s in 1:length(state.vars)) {
    state.levels[[s]] <- sort(unique(data[,state.vars[s]]))
    nv[s] <- length(state.levels[[s]])
  }
  si <- function(state) {
    stopifnot(length(state)==length(nv))
    sn <- as.numeric(state)
    fac <- 1
    index <- 0
    for (i in 1:length(nv)) {
      index <- index + (sn[i]-1)*fac
      fac <- fac*nv[i]
    }
    return(index+1)
  }

  sv <- function(index) {
    stopifnot(index<=n.states)
    state <- data[1,state.vars]
    li <- rep(NA,length(state.vars))
    fac <- prod(nv)
    index <- index - 1
    for (i in 1:length(nv)) {
      li[i] <- index %% nv[i] + 1
      index <- index %/% nv[i]
      state[[i]] <- state.levels[[i]][li[i]]
    }
    state[[1]] <- state.levels[[1]][li[1]]
    return(state)
  }
  return(list(index=si, vector=sv))
}

state.fn <- vector.index.converter(plantData,state.vars)
action.fn <- vector.index.converter(plantData, action.vars)

# state.fn$vector(i) returns a state vector with index i
# state.fn$index(state) returns index for state vector 
\end{lstlisting}
  The file
  \href{https://bitbucket.org/paulschrimpf/econ565/src/master/assignments/rustRothwell1996/rustRothwell.R}{rustRothwell.R}
  Contains code including the vector to index conversion above to
  calculate the matrix of flow utility, array of transition
  probabilities, value functions, and likelihood. 
  \begin{enumerate}
  \item Check the value function by verifying that the choice specific
    value varies sensibly with the state and action. You can use the
    value of $\phi$ estimated by Rust and Rothwell. (Their estimates
    include month dummies, but we will leave them out). You should
    recreate something like figures 13 and 14. 
  \item Unfortunately, the likelihood function as written is very
    slow. To identify what part of the code is taking the most time,
    we can use R's profiler. 
\begin{lstlisting}
Rprof("rrLike.prof", line.profiling=TRUE) # start the profiler
likelihood(phi.post)
Rprof(NULL) # stop the profiler

summaryRprof("rrLike.prof") # show the results
\end{lstlisting}
    What part of the likelihood function is slow? Can you speed it up?
  \item 
    The slowest part of the likelihood is calculating the choice
    specific value functions. I rewrote the function in C++ in the file
    \href{https://bitbucket.org/paulschrimpf/econ565/src/master/assignments/rustRothwell1996/choiceValue.cpp}{choiceValue.cpp}. You
    need not understand this code. To compile the code on Windows, you
    must first install (separately not as an R
    package) the \href{http://cran.r-project.org/bin/windows/Rtools/}{Rtools program}. 
    The
    first half of the file
    \href{https://bitbucket.org/paulschrimpf/econ565/src/master/assignments/rustRothwell1996/rr-estimate.R}{rr-estimate.R} compiles
    the C++ code and makes it callable from R. It creates a function
    \texttt{choiceValue(u,feasible.action,discount,P,T)} which should
    give the same result as the choice.value function written in R. 
    Profile the likelihood using the C++ version of choiceValue and
    compare how long it takes.
  \end{enumerate}
\end{problem}

\begin{problem}[Estimation]
  Now with a likelihood that hopefully does not take too long to
  evaluate, we can try to maximize the likelihood. The second half of
  \href{https://bitbucket.org/paulschrimpf/econ565/src/master/assignments/rustRothwell1996/rr-estimate.R}
  {rr-estimate.R} 
  attempts to do so.  
  \begin{enumerate}
  \item Explore the sensitivity of the maximization to initial value
    and/or the choice of optimization algorithm. How confident are
    you in the results? How do the results compare to those of
    \cite{rr1995}? (The results will not be the same because we did not split
    the sample into pre and post Three Mile Island, did not include
    month dummies, have different estimated transition probabilities, etc).
  \item Assess the fit the estimates by producing something like
    figures 11 and 12 of \cite{rr1995}.
  \end{enumerate}
\end{problem}


\bibliographystyle{jpe}
\bibliography{../../notes/565}

\end{document}
