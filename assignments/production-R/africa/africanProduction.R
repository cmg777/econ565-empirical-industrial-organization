## R code for estimating production functions for some African
## manufacturing firms
source("downloadData.R")

################################################################################
# OLS estimate of production function
require(plm) ## package for panel data of
require(lmtest) ## for coeftest
require(sandwich)
source("clusterFunctions.R")
df$Materials <- df$materials + df$indirect.costs
# formula specifying the production function, you could add controls
# for year, country, industry, etc
fmla <- log(output) ~ log(capital) + log(labor) + log(Materials) +
  log(indirect.costs)

summary(lm(fmla,data = df))
# clustered standard errors
prod.ols <- plm(fmla, data=df,model="pooling", index=c("firm","year"))
cl.plm(df, prod.ols, df$firm)
# fixed effects
prod.fe <- plm(fmla, data=df, model="within", index=c("firm","year"))
cl.plm(df, prod.fe, df$firm)


# create investment = change in capital
# Note: capital (as well as output, Materials, and indirect costs) is
# measured in constant (1991) US dollars, so even if a firm did
# nothing, it will look like capital changed from inflation.
df <- df[order(df$firm, df$year), ] # sort data
df$invest <- c(df$capital[2:nrow(df)] - df$capital[1:(nrow(df)-1)],NA)
df$invest[c(df$firm[2:nrow(df)]!=df$firm[1:(nrow(df)-1)],FALSE)] <- NA


## Olley-Pakes estimation
# Step 1
notmissing <- !is.na(df$capital) & !is.na(df$invest)
df.nm <- subset(df,notmissing)
df.nm <- df.nm[order(df.nm$firm, df.nm$year),]

op1 <- lm(log(output) ~ log(labor) + log(Materials) +
          poly(cbind(log(capital),invest),degree=4),
          data=df.nm)
b1 <-op1$coefficients[c("log(labor)", "log(Materials)")]
xb1 <- log(as.matrix(df.nm[,c("labor", "Materials")])) %*% b1
fhat <- predict(op1,df.nm) - xb1

# Step 2
lag <- function(x, i=df.nm$firm, t=df.nm$year) {
  if (length(i) != length(x) || length(i) != length(t) ) {
    stop("Inputs not same length")
  }
  x.lag <- x[1:(length(x)-1)]
  x.lag[i[1:(length(i)-1)]!=i[2:length(i)] ] <- NA
  x.lag[t[1:(length(i)-1)]+1!=t[2:length(i)] ] <- NA
  return(c(NA,x.lag))
}

# create data frame for step 2 regression
df.step2 <- data.frame(lhs=(log(df.nm$output)-xb1),
                       k=log(df.nm$capital),fhat=fhat,
                       k.lag=log(lag(df.nm$capital)),
                       f.lag=lag(fhat))
# drop missing observations because they mess up poly()
df.step2 <- subset(df.step2, !apply(df.step2, 1, function(x)
                                    any(is.na(x))))
# objective function = sum of residuals^2
objective <- function(betaK, degree=4) {
  op2 <- lm(I(lhs - betaK*k) ~ poly(I(f.lag - betaK*k.lag),degree),
            data=df.step2)
  return(sum(residuals(op2)^2))
}

## plot objective
fig.df <- data.frame(bk=seq(from=-0.02,to=0.3, by=0.005))
fig.df$obj <- sapply(fig.df$bk, objective)
library(ggplot2)
ggplot(data=fig.df,aes(x=bk,y=obj)) + geom_point()


## minimize objective
opt.out <- optim(prod.ols$coefficients["log(capital)"],
                 fn=objective,method="Brent",lower=-1,upper=1)

source("~/productionEstimation/productionEstimation.R")
debug(production.cf.2step)
df$t <- df$year
df$id <- df$firm
cf.inv <- production.cf.2step(log(output) ~ log(labor) + log(Materials)
                              + log(capital),
                              ~ invest + log(capital),
                              data=df, degree=3)
# treat labor as fixed
cf.inv2 <- production.cf.2step(log(output) ~ log(labor) + log(Materials)
                              + log(capital),
                               ~ invest + log(capital) + log(labor),
                              data=df, degree=3)

# use materials
cf.mat <- production.cf.2step(log(output) ~ log(labor) + log(Materials)
                              + log(capital),
                              ~ log(Materials) + log(capital) + log(labor),
                              data=df, degree=3,
                              predetermined=c("log(capital)","log(labor)"))

